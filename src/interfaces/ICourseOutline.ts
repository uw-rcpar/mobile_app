import {ISection} from "./ISection";

export interface ICourseOutline {
  sections: Array<ISection>
}
