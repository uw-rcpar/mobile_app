export interface IProgressPercent {
  sku: string,
  percentage: number
}
