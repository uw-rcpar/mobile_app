export interface IVideoUrl {
  bitrate: string,
  url: string
}
