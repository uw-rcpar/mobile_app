export interface IProfile {
  display: string,
  first: string,
  last: string,
  mail: string,
  uid: string,
}
