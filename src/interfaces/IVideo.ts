export interface IVideo {
  id: string,
  playbackTime: number,
  timestamp: number,
  _synchronized: boolean,
}
