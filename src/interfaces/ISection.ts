import {IChapter} from "./IChapter";
export interface ISection {
  section: string, // "AUD, BEC , FAR, REG"
  title: string,
  chapters: Array<IChapter>,
  type: string, // 'regular_course | cram_course'
  total_time: number, // milliseconds
  enabled?:boolean,// extra info added by the app
}
