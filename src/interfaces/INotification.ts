export interface INotification {
  body:string,
  created_date:string,
  nid:string;
  teaser:string,
  title:string,
  type:string,
  status: string;
}
