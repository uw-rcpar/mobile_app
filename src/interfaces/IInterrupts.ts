export interface IInterrupts {
  review: string ,
  cram: string ,
  download: string,
  flashcards: string,
  audio: string
}
