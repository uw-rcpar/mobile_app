import {IVideoUrl} from "./IVideoUrl";
export interface ITopic {
  id: string,
  title: string,
  slug: string,
  pdf_url?: string,
  video_src: string,
  video_file: string,
  video_id: string,
  duration: number,
  video_urls: {low: IVideoUrl, mid: IVideoUrl, high: IVideoUrl}
}
