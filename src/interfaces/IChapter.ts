import {ITopic} from "./ITopic";
export interface IChapter {
  id: string,
  title: string,
  slug: string,
  delta: string,
  topics: Array<ITopic>
}
