import {IVideo} from "./IVideo";
export interface  IVideos {
  [videoId: string]: IVideo
}
