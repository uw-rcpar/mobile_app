export interface IDownload {
  title: string,
  topicId: string,
  sectionId: string,
  courseType: string,
  video: {
    videoId: string,
    localFilename: string,
    url: string,
    status: number,
    localUrl?: string,
    type: string
  },
  pdf: {
    localFilename: string,
    url: string,
    status: number,
    localUrl?: string
  },

}
