import {IEntitlement} from "./IEntitlement";
import {IProfile} from "./IProfile";
import {IInterrupts} from "./IInterrupts";

export interface IUser {
  token: string,
  tokenExpiration: Date,
  entitlements?: IEntitlement [],
  profile:IProfile,
  interrupts?: IInterrupts
}
