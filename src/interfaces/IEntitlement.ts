export interface IEntitlement {
  name:string;
  id: string;
  sku: string;
  exam_version: string;
  content_restricted: boolean;
  restrictions: string[];
  expiration: string;
  section: string;
  type: string;
  valid: boolean;
  offline_access: boolean;
  activated: true;
}
