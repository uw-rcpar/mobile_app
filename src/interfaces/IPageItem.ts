export interface IPageItem {
  id: string,
  title: string,
  description: string,
  component: any,
  icon: string,
  root: boolean,
  entitlementTypes?: string[], // Refers to the entitlement.type that allow accessing into the page
  enabled?: boolean,
  activated?: boolean,
  params?: any
}
