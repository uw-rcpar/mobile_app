import {Injectable} from '@angular/core';
import {Network} from "@ionic-native/network";
import {Observable} from "rxjs/Observable";
import "rxjs";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class NetworkProvider {

  public change: BehaviorSubject<string>;

  constructor(private network: Network) {
    this.change = new BehaviorSubject<string>(this.hasNetwork);
    this.network.onchange().map((event) => {
      //console.log(event)
      this.change.next(event)
    });
  }

  get hasNetwork(){
    if (this.network.type === null || this.network.type && this.network.type != 'none'){
      return "online";
    }else{
      return "offline"
    }
  }



}
