import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Environment} from "../../environments/env";
import {Observable} from "rxjs/Observable";
import {IUser} from "../../interfaces/IUser";
import {IEntitlement} from "../../interfaces/IEntitlement";
import {RequestOptions} from '@angular/http';


@Injectable()
export class UserLoginApiProvider {
  BASE = "user"

  constructor(public http: HttpClient) {
  }

  public login(loginUser: { email: string, password: string }): Observable<IUser> {
    if (loginUser.email === null || loginUser.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      const body = new HttpParams()
        .set('name', loginUser.email)
        .set('pass', loginUser.password);
      const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
      return this.http.post<any>(Environment.API_ENDPOINT + this.BASE + '/login', body.toString(), {headers}).map((response) => {
        if (response.code && response.code >= 401) {
          throw response;
        } else {
          return {...response, email: loginUser.email, entitlements: this.fixEntitlements(response.entitlements)}
        }
      });
    }
  }


  public refreshToken() : Observable<IUser> {
    return this.http.get<any>(Environment.API_ENDPOINT + this.BASE + '/login').map((response) => {
      if (response.code && response.code >= 401) {
        throw response;
      } else {
        let email =  (response && response.profile) ? response.profile.mail : null ;
        return {...response, email: email,  entitlements: this.fixEntitlements(response.entitlements)}
      }
    });
  }

  private fixEntitlements(entitlements: any): IEntitlement[] {
    let fixedEntitlments: IEntitlement[] = []
    for (let k of Object.keys(entitlements)) {
      fixedEntitlments.push(entitlements[k]);
    }
    return fixedEntitlments;
  }

}
