import {Injectable} from '@angular/core';
import {IUser} from '../../interfaces/IUser';
import {UserLoginApiProvider} from "./user-login-api";
import {UserLoginLocalProvider} from "./user-login-local";
import {Subject} from "rxjs/Subject";
import {ISection} from "../../interfaces/ISection";
import {DateTime} from "ionic-angular";
import {tokenReference} from "@angular/compiler";


@Injectable()
export class UserLoginProvider {
  public user$ = new Subject<IUser>();
  public user: IUser;
  public firstLogin = true;
  public tokenRefreshedOn: Date;

  // Minimun time (in seconds) to keep the current token and entitlement list
  private REFRESH_TOKEN_TIME = 60;

  public isLoggedIn(): boolean {
    return this.user && this.user.token ? true : false;
  }

  public login(user: {email: string, password: string}) {
    return this.userLoginApi.login(user).do((user: IUser) => {
      this.save(user);
      this.firstLogin = false;
    });
  }

  /**
   * Defines if a token / entitlements should be refreshed
   * @returns {boolean}
   */
  public shouldRefreshToken() {
    if (this.tokenRefreshedOn) {
      const now = new Date();
      return  ( now.getTime() - this.tokenRefreshedOn.getTime() )  > this.REFRESH_TOKEN_TIME * 1000  ;
    }
    return true;
  }

  /**
   * Refresh user token and entitlements from the server
   * @returns {Observable<T>}
   */
  public refreshToken() {
    return this.userLoginApi.refreshToken().do((user: IUser) => {
      this.tokenRefreshedOn = new Date();
      console.log("TOKEN REFRESHED ON",this.tokenRefreshedOn);
      this.save(user);
    }).toPromise();
  }

  public logout(): void {
    this.user$.next(null);
    this.userLoginLocal.delete();
  }

  private save(user: IUser): void {
    this.user$.next(user);
    this.userLoginLocal.save(user);
  }

  constructor(private userLoginApi: UserLoginApiProvider, private userLoginLocal: UserLoginLocalProvider) {
    this.userLoginLocal.get().then((user) => {
        this.user$.next(user)
      }
    )

    this.user$.subscribe(user => {
      this.user = user;
      if (user) {
        this.firstLogin = false
      }
    });
  }


  // /**
  //  * Local entitlement validate - sets a valid/invalid flag based on expirtation date
  //  * @returns {Promise<void>}
  //  */
  public validateEntitlements(): IUser {
    if (this.user) {
      this.user.entitlements = this.user.entitlements.map((entitlement) => {
        if (new Date(Date.parse(entitlement.expiration)) < new Date()) {
          entitlement.valid = false;
        } else {
          entitlement.valid = true;
        }
        return entitlement;
      });
      this.save(this.user)
      return this.user;
    }
  }


  public getInterruptPageUrl(section: string, feature: string) {
    if (this.user.interrupts[feature]) {
      let url = this.user.interrupts[feature];
      url = url.replace('<SECTION>', section);
      url = url.replace('<UID>', this.user.profile.uid);
      return url;
    }
    return null;
  }

  /**
   * Permissions check for offline access
   * @param section
   * @returns {IEntitlement|any|boolean}
   */
  public hasOfflineAccessPermissions(section: ISection): boolean {
    if (section) {
      const entitlement = this.findEntitlementBySection(section);
      //console.log('hasOfflineAccessPermissions', section, entitlement, entitlement && entitlement.activated && entitlement.offline_access);
      return entitlement && entitlement.offline_access;
    }
    return false;
  }


  /**
   * Gets the entitlement for the section
   * @param section
   * @returns {undefined|IEntitlement}
   */
  public findEntitlementBySection(section: ISection) {
    let entitlements = this.user.entitlements;
    let suffix = '';
    if (section.type == 'cram_course') {
      suffix = '-CRAM';
    }
    let sku = section.section + suffix;
    return entitlements.find((en) => en.sku == sku);
  }

  public hasRestrictedTopics(section: ISection): boolean {
    let entitlement = this.findEntitlementBySection(section);
    return entitlement && entitlement.content_restricted;
  }

  /**
   * Returns the list of topic ids that the user is allowed in case has content_restructed == true
   * @param section
   * @returns {string[]}
   */
  public getTopicRestrictions(section: ISection): string[] {
    let entitlement = this.findEntitlementBySection(section);
    return (entitlement) ? entitlement.restrictions : [];
  }
}
