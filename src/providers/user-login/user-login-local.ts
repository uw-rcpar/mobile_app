import {Injectable} from '@angular/core';
import {IUser} from "../../interfaces/IUser";
import {Storage} from '@ionic/storage';

@Injectable()
export class UserLoginLocalProvider {

  public save(user: IUser): void {
    this.storage.set('user', user);
  };

  public get(): Promise<IUser> {
    return this.storage.get('user');
  }

  public delete() {
    this.storage.remove("user")
  }

  constructor(private storage: Storage) {
  }

}
