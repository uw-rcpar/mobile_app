import {Injectable} from '@angular/core';
import {DataProvider} from "../data/data";
import {File} from "@ionic-native/file";
import {Platform} from "ionic-angular";
import {IDownload} from "../../interfaces/IDownload";
import {UserLoginProvider} from "../user-login/user-login";

@Injectable()
export class FileManagerProvider {

  public downloadingTopics = {};
  public downloadedTopics = {};
  private DOWNLOADED_VIDEO_MIN_SIZE = 1000; // min size bytes

  constructor(private file: File, private dataProvider: DataProvider, private platform: Platform, private userLoginProvider: UserLoginProvider) {
  }


  public init() {
    // Save download status
    this.downloadedTopics = {};
    this.downloadingTopics = {};
    this.dataProvider.getTopicDownloads().then((downloads) => {
      downloads.forEach((download: IDownload) => {
        this.downloadedTopics[download.topicId] = true;
        if (download.video.status == DataProvider.TOPIC_DOWNLOAD_STATUS_PENDING) {
          this.downloadingTopics[download.topicId] = true;
        }
      });
    });
  }


  public removeDownloadList(topicIds) {

    let promises = [];

    for (let topicId of topicIds) {
      //console.log('removeDownload', topicId);
      this.downloadedTopics[topicId] = false;
      this.downloadingTopics[topicId] = false;
      let promise = this.dataProvider.getSingleTopicDownlad(topicId).then((download) => {
        if (download) {
          //console.log('removedDownload from DB');
          let videoFileUrl = decodeURI(download.video.localUrl);
          let pdfFileUrl = decodeURI(download.pdf.localUrl);
          let fileRemovePromises = [];
          //console.log(videoFileUrl);
          if (videoFileUrl) {
            //console.log(videoFileUrl);
            let promise = this.file.removeFile(
              this.getUrlPath(videoFileUrl),
              this.getUrlFilename(videoFileUrl)
            ).then(() => {
              //console.log('file removed', videoFileUrl);
            }, () => {
              //console.log('error removing file', videoFileUrl);
            });
            fileRemovePromises.push(promise);

          }
          if (pdfFileUrl) {
            let promise = this.file.removeFile(
              this.getUrlPath(pdfFileUrl),
              this.getUrlFilename(pdfFileUrl)
            ).then(() => {
              //console.log('file removed', pdfFileUrl);
            }, () => {
              //console.log('error removing file', pdfFileUrl);
            });
            fileRemovePromises.push(promise);
          }
          // Fullfill when all files are removed from filesystem
          //console.log("all files removed")
          return Promise.all(fileRemovePromises);
        } else {
          throw ('download_not_found');
        }
      });
      promises.push(promise);
    }

    return Promise.all(promises).then(() => {
      return this.dataProvider.removeTopicDownloadList(topicIds)
    })
  }


  public removeDownload(topicId) {
    //console.log('removeDownload', topicId);
    this.downloadedTopics[topicId] = false;
    this.downloadingTopics[topicId] = false;
    return this.dataProvider.getSingleTopicDownlad(topicId).then((download) => {
      if (download) {
        return this.dataProvider.removeTopicDownload(download.topicId).then(() => {
          //console.log('removedDownload from DB');
          let videoFileUrl = decodeURI(download.video.localUrl);
          let pdfFileUrl = decodeURI(download.pdf.localUrl);
          let fileRemovePromises = [];
          //console.log(videoFileUrl);

          if (videoFileUrl) {
            //console.log(videoFileUrl);
            let promise = this.file.removeFile(
              this.getUrlPath(videoFileUrl),
              this.getUrlFilename(videoFileUrl)
            ).then(() => {
              //console.log('file removed', videoFileUrl);
            }, () => {
              //console.log('error removing file', videoFileUrl);
            });
            fileRemovePromises.push(promise);

          }
          if (pdfFileUrl) {
            let promise = this.file.removeFile(
              this.getUrlPath(pdfFileUrl),
              this.getUrlFilename(pdfFileUrl)
            ).then(() => {
              //console.log('file removed', pdfFileUrl);
            }, () => {
              //console.log('error removing file', pdfFileUrl);
            });
            fileRemovePromises.push(promise);
          }
          // Fullfill when all files are removed from filesystem
          //console.log("all files removed")
          return Promise.all(fileRemovePromises);
        });
      } else {
        throw ('download_not_found');
      }
    });
  }

  public getUrlPath(str) {
    return str.substring(0, str.lastIndexOf("/"));
  }

  public getUrlFilename(str) {
    return str.substring(str.lastIndexOf('/') + 1);
  }

  /**
   * Returns the directory to store downloads depending on the platform
   * https://github.com/apache/cordova-plugin-file#where-to-store-files
   * @returns {string}
   */
  public getDownloadPath() {
    let path;
    if (this.platform.is('ios')) {
      //path =  this.file.dataDirectory ;
      path = this.file.dataDirectory;
      //console.log("is ios,  use download path: ", path);
    } else if (this.platform.is('android')) {
      // fot some reason on android doenst play videos stored at file.dataDirectory
      path = this.file.externalApplicationStorageDirectory;
      //console.log("is android,  use download path: ", path)
    } else {
      // default
      path = this.file.dataDirectory;
    }
    return path ? path : '/';
  }

  public cleanup(): Promise<any> {
    if (this.platform.is('mobileweb') || this.platform.is('core')) {
      return Promise.resolve(false);
    } else {
      return this.file.removeRecursively(this.getDownloadPath(), 'section');
    }
  }

  /**
   * Downloads directory structure:
   * section/aud/regular/{topic-id}.mp4
   * section/aud/regular/{topic-id}.pdf
   */
  public createDirectoryStructure(): Promise<any> {
    return this.file.createDir(this.getDownloadPath(), 'section', true);
  }


  /**
   * Return Section Directory structure like 'section/aud/'
   * @param sectionId
   * @returns {string}
   */
  private buildSectionDirectoryPath(sectionId): string {
    return this.getDownloadPath() + this.userLoginProvider.user.profile.uid + '/section/' + sectionId + '/';
  }

  /**
   * Returns path in this format: 'section/aud/regular/
   * @param sectionId
   * @param courseType
   * @returns {string}
   */
  private buildCourseDirectoryPath(sectionId, courseType: string): string {
    return this.buildSectionDirectoryPath(sectionId) + courseType + '/';
  }

  /**
   * Returns path in this format: 'section/aud/regular/{topic-id}.mp4'
   * @param sectionId
   * @param courseType
   * @param topicId
   * @param fileExtension
   * @returns {string}
   */
  public buildTopicFilePath(sectionId, courseType, topicId, fileExtension: string): string {
    if (!sectionId) {
      console.error('Section Id is null');
    }
    return this.buildCourseDirectoryPath(sectionId, courseType) + topicId + '.' + fileExtension;
  }

  /**
   * Validates the video metadata
   * Check video size
   * @param metadata
   * @returns {any|boolean}
   */
  public validateVideoMetadata(metadata): boolean {
    return metadata && metadata.size > this.DOWNLOADED_VIDEO_MIN_SIZE;
  }
}


