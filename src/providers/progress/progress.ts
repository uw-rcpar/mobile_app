import {Injectable} from '@angular/core';
import {ICourseOutline} from "../../interfaces/ICourseOutline";
import {IVideo} from "../../interfaces/IVideo";
import {ISection} from "../../interfaces/ISection";
import {IVideos} from "../../interfaces/IVideos";
import {UserLoginProvider} from "../user-login/user-login";
import {Environment} from "../../environments/env";
import {HttpClient} from "@angular/common/http";


@Injectable()
export class ProgressProvider {
  private BASE = "course/progress";

  constructor(public http: HttpClient, private userLoginProvider: UserLoginProvider) {
  }


  fetchSectionProgress(sectionSku: string[]) {
    let params = [];
    for (var i in sectionSku) {
      params.push("sku[]=" + sectionSku[i]);
    }
    let paramsStr = params.join("&");
    return this.http.get(Environment.API_ENDPOINT + this.BASE + "?" + paramsStr).toPromise();
  }


  /**
   * @deprecated
   *
   * @param sectionSku
   * @param courseOutline
   * @param videosProgress
   * @param type
   * @returns {number}
   */
  calculateSectionProgress(sectionSku: string,
                           courseOutline: ICourseOutline,
                           videosProgress: IVideos,
                           type: string): number {

    const section: ISection = courseOutline.sections.find((s) => s.section == sectionSku && s.type == type);
    const hasRestrictedTopics = this.userLoginProvider.hasRestrictedTopics(section);
    const topicRestrictions = this.userLoginProvider.getTopicRestrictions(section);

    //console.log('PROGRESS: -------------- START --------------');
    //console.log('PROGRESS: calculateSectionProgress', section.section + '-' + section.type);
    //const maxSectionTime = +section.total_time;
    let maxSectionTime = 0;
    let sectionTime: number = 0;
    section.chapters.forEach((chapter) => {
      chapter.topics.forEach((topic) => {
        if (!hasRestrictedTopics || topicRestrictions.indexOf(topic.id) != -1) {
          let videoTime = 0;
          maxSectionTime += +topic.duration;
          if (videosProgress[topic.video_id]) {
            //console.log('PROGRESS:  calculateSectionProgress Topic video id = ' + topic.video_id);
            const videoTime = +videosProgress[topic.video_id].playbackTime;
            //console.log('PROGRESS:  videoTime  = ' + videoTime);
            //console.log('PROGRESS:  topicDuration  = ' + topic.duration );
            //console.log('PROGRESS:  @maxSectionTotalTime  = ' + maxSectionTime);

            // '+' unary operator to convert string to integer
            if (videoTime) {
              sectionTime += videoTime;
            }
          } else {
            // console.log('video id not found in local progress ' + topic.video_id);
          }
          sectionTime += videoTime;
        }
      });
    });
    // console.log('PROGRESS: section time = ' + sectionTime);
    // console.log('PROGRESS: sectionMax time = ' + maxSectionTime);
    let progressPercent = Math.round(sectionTime * 100 / maxSectionTime);
    //console.log('PROGRESS: percent = ' + progressPercent);
    return progressPercent;
  }

}
