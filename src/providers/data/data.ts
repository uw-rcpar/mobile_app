import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable' ;
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do'
import {Storage} from "@ionic/storage";
import {IEntitlement} from "../../interfaces/IEntitlement";
import {ISection} from "../../interfaces/ISection";
import {UserLoginProvider} from "../user-login/user-login";
import {ICourseOutline} from "../../interfaces/ICourseOutline";
import {Platform} from "ionic-angular";
import {ITopic} from "../../interfaces/ITopic";
import {Headers, RequestOptions, URLSearchParams} from "@angular/http";
import {HttpHeaders} from "@angular/common/http";
import {IVideo} from "../../interfaces/IVideo";
import {IChapter} from "../../interfaces/IChapter";
import {HttpParams} from "@angular/common/http";
import {Network} from "@ionic-native/network";
import {Environment} from "../../environments/env";


export class ApiResponseError {
  public error: string;
}

/*
 Data Provider. Responsible to manage course related data.
 */
@Injectable()
export class DataProvider {


  static readonly COURSE_OUTLINE_ENDPOINT = "course/outline";
  static readonly VIDEO_SYNC_ENDPOINT = "video/sync";
  static readonly TOPIC_DOWNLOAD_STATUS_DOWNLOADED = 2;
  static readonly TOPIC_DOWNLOAD_STATUS_PENDING = 1;

  private baseAPIPath: string; // may change on dev/prod so its no readonly

  private courseOutline: ICourseOutline;
  private currentSection: string;
  private isApp: boolean;

  constructor(public http: HttpClient, public storage: Storage, private userLoginProvider: UserLoginProvider,
              private network: Network) {
    this.isApp = (<any>window).cordova ? true : false;
    this.baseAPIPath = Environment.API_ENDPOINT;
  }


  /**
   * Performs a http request from course endpoint and saves is to local storage
   * @returns {any}
   */
  public syncCourseOutline(): Promise<any> {
    let courseOutline: ICourseOutline = {sections: []};
    //console.log('sync course outline');
    return this.http.get(this.baseAPIPath + DataProvider.COURSE_OUTLINE_ENDPOINT).toPromise().then((data: any) => {
      //console.log('course outline response');
      //console.log(data);
      if (data && data.sections) {
        courseOutline.sections = data.sections;
        return this.storage.set(this.userLoginProvider.user.profile.uid + '_courseOutline', courseOutline);
      } else {
        if (data.code == "401") {
          this.userLoginProvider.logout();
          throw 'not authorized';
        } else {
          throw 'invalid data';
        }
      }
    });
  }

  /**
   * Retrieves course outline from local storage
   * @returns {any}
   */
  public getCourseOutline(): Promise<ICourseOutline> {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_courseOutline').then((courseOutline: ICourseOutline) => {
      // store it locally
      if(courseOutline){
        this.courseOutline = courseOutline;
        return courseOutline;
      }else{
        return this.syncCourseOutline().then(courses =>{
          this.courseOutline = courses;
          return courses;
        })
      }

    });
  }

  /**
   * updates the current sections list variable with the saved in local storage
   */
  public getUserAllowedSections(): Promise<ISection[]> {

    return this.getCourseOutline().then((data: ICourseOutline) => {
      let staticSections: Array<ISection> = data.sections;
      // Fill static section with enabled:boolean flag according to current entitlements
      let sectionsWithPermissions = staticSections.map((section: ISection) => {
        section.enabled = false;
        if (this.userLoginProvider.isLoggedIn()) {
          if (this.isSectionAllowedInEntitlements(this.userLoginProvider.user.entitlements, section)) {
            section.enabled = true;
          }else {
            console.log('section not allowed', section);

          }
        }
        return section;
      });
      //console.log(sectionsWithPermissions);
      return sectionsWithPermissions;
    });
  }


  /**
   * Checks if a section is allowed ounder entitilements list
   * @param IEntitlement
   */
  public isSectionAllowedInEntitlements(entitlements: IEntitlement[], section: ISection): boolean {
    let entitlement = entitlements.find((ent) => ent.section == section.section);
    return entitlement ? true : false;
  }


  public setCurrentSectionSku(section: string) {
    // Update scope
    this.currentSection = section;
    // Update saved state
    this.storage.set(this.userLoginProvider.user.profile.uid + '_currentSection', section);
  }

  /**
   * updates the current section scope variable with the saved in local storage
   * @returns {Promise<TResult>|Promise<any>|Promise<TResult2|TResult1>}
   */
  public loadCurrentSectionSku(): Promise<string> {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_currentSection').then(section => {
      if (!section) {
        section = "AUD";
      }
      this.currentSection = section;
      return section;
    });
  }


  public getSectionBySkuAndType(sectionSku: string, type: string): ISection {
    return this.courseOutline.sections.find(s => s.section == sectionSku && s.type == type);
  }


  /**
   * Returns current topic for given section
   * If none exists, return the first one
   * "uiState" : {
   *    sections: {
   *      "AUD" {
   *         "currentVideoId" : "AUD-Cram-002"
   *       }
   *    }, ...
   * }
   * @param video
   */
  public loadCurrentTopic(section: ISection, defaultFirstChapter: boolean): Promise<ITopic> {
    let sectionSku = section.section;
    let sectionType = section.type;
    let videoId;
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_uiState').then((uiState) => {
      // try to find currentVideo Id stored
      uiState = uiState || {};
      uiState.sections = uiState.sections || {};
      if (uiState.sections[sectionSku]) {

        if (uiState.sections[sectionSku][section.type]) {
          videoId = uiState.sections[sectionSku][section.type].currentVideoId;
        } else {
          // Old version saves on root section sku
          //videoId = uiState.sections[sectionSku].currentVideoId;
        }
        if (videoId) {
          return this.findTopicByVideoId(videoId);
        }
      }
      // Stored video not found. Get the first one on the current section
      //console.log('loadingCurrentTopic as first video on section', section);
      if (defaultFirstChapter) {
        return section.chapters[0].topics[0];
      }
      return null;

    });
  }


  /**
   * Returns the current topic for the section sky
   * @param sectionSku
   */
  public loadCurrentTopicForSectionSku(sectionSku: string, defaultFirstChapter: boolean) {
    return this.loadCurrentTopic(this.findSectionBySkuAndType(sectionSku, 'regular_course'), defaultFirstChapter);
  }

  /**
   * Sets current video for the given section
   * @param video
   */
  public setCurrentVideoId(section: ISection, videoId) {
    if (videoId) {
      let sectionId = section.section;
      let sectionType = section.type;
      return this.storage.get(this.userLoginProvider.user.profile.uid + '_uiState').then((uiState) => {
        uiState = uiState || {};
        uiState.sections = uiState.sections || {};
        uiState.sections[sectionId] = uiState.sections[sectionId] || {};
        uiState.sections[sectionId][sectionType] = uiState.sections[sectionId][sectionType] || {};
        uiState.sections[sectionId][sectionType].currentVideoId = videoId;
        return this.storage.set(this.userLoginProvider.user.profile.uid + '_uiState', uiState);
      });
    }
  }

  public findTopicByVideoId(videoId): ITopic {
    let found = null;
    this.courseOutline.sections.every(function (section: ISection) {
      section.chapters.every(function (chapter) {
        found = chapter.topics.find(function (topic) {
          return topic.video_id == videoId;
        });
        return !found;
      });
      return !found;
    });
    return found;
  }


  public findTopicById(topicId: string): ITopic {
    let found = null;
    this.courseOutline.sections.every(function (section: ISection) {
      section.chapters.every(function (chapter) {
        found = chapter.topics.find(function (topic) {
          return topic.id == topicId;
        });
        return !found;
      });
      return !found;
    });
    return found;
  }


  public findSectionByTopicId(topicId): ISection {
    let found = null;
    let result = null;
    this.courseOutline.sections.every(function (section: ISection) {
      section.chapters.every(function (chapter) {
        found = chapter.topics.find(function (topic) {
          return topic.id == topicId;
        });
        if (found) {
          result = section;
        }
        return !found; // if found, then break execution
      });
      return !found;
    });
    return result;
  }

  public findSectionByVideoId(videoId): ISection {
    let found = null;
    let result = null;
    this.courseOutline.sections.every(function (section: ISection) {
      section.chapters.every(function (chapter) {
        found = chapter.topics.find(function (topic) {
          return topic.video_id == videoId;
        });
        if (found) {
          result = section;
        }
        return !found; // if found, then break execution
      });
      return !found;
    });
    return result;
  }

  public findSectionBySkuAndType(sku: string, type: string): ISection {
    //console.log(sku, type);
    return this.courseOutline.sections.find((s) => s.type == type && s.section == sku);
  }

  public findChapterByTopicId(topicId: string): IChapter {
    let foundChapter: IChapter = null;
    let foundTopic: ITopic = null;
    this.courseOutline.sections.every(function (section: ISection) {
      section.chapters.every(function (chapter) {
        let foundTopic = chapter.topics.find(function (topic) {
          return topic.id == topicId;
        });
        if (foundTopic) {
          foundChapter = chapter;
        }
        return foundTopic == null; // if found, then break execution
      });
      return foundTopic == null;
    });
    return foundChapter;
  }

  /**
   *
   * @param topicId
   * @param type
   * @param title
   * @param sectionId
   * @param courseType
   * @param video
   * @param pdf
   */
  public saveDownload(topicId: string, type: string, title: string, sectionId: string, courseType: string, video, pdf) {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_topicDownloads').then((topicDownloads) => {
      topicDownloads = topicDownloads || [];
      let entryIndex: number = topicDownloads.findIndex((el: any) => el && (el.topicId == topicId));
      let entry: any;
      let isNew;
      if (entryIndex == -1) {
        // its new: perform insert create object instance
        entry = {
          video: {},
          pdf: {}
        };
        isNew = true;
      } else {
        entry = topicDownloads[entryIndex];
        isNew = false;
      }
      entry.topicId = topicId;
      entry.sectionId = sectionId;
      entry.courseType = courseType;
      entry.title = title;
      if (type == 'video') {
        entry.video = video;
      } else if (type == 'pdf') {
        entry.pdf = pdf;
      }
      if (isNew) {
        topicDownloads.push(entry);
      } else {
        topicDownloads[entryIndex] = entry;
      }
      //console.log('saving Download', entry);
      return this.storage.set(this.userLoginProvider.user.profile.uid + '_topicDownloads', topicDownloads);
    });
  }

  /**
   *
   * Get all Downloaded Topics saved into local storage
   * @param sectionId - if sectionId <> null filters by the given section
   * @returns Promise<any>
   */
  public getTopicDownloads(sectionId?) {
    if (this.userLoginProvider.user) {
      return this.storage.get(this.userLoginProvider.user.profile.uid + '_topicDownloads').then((topicDownloads) => {
        //console.log('topicDownloads', topicDownloads);
        topicDownloads = topicDownloads || [];
        if (sectionId) {
          topicDownloads = topicDownloads.filter((item) => item && item.sectionId == sectionId);
        }
        return topicDownloads;
      });
    }
  }

  public getSingleTopicDownlad(topicId) {
    return this.getTopicDownloads(null).then((downloads) => {
      return downloads.find((item) => item && item.topicId == topicId);
    });
  }


  public removeTopicDownloadList(topicIds) {
    return this.getTopicDownloads(null).then((topicDownloads) => {
      topicDownloads = topicDownloads.filter((el: any) => el && (topicIds.indexOf(el.topicId) == -1));
      return this.storage.set(this.userLoginProvider.user.profile.uid + '_topicDownloads', topicDownloads);
    });
  }

  public removeTopicDownload(topicId) {
    return this.getTopicDownloads(null).then((topicDownloads) => {
      let entryIndex: number = topicDownloads.findIndex((el: any) => el && (el.topicId == topicId));
      delete topicDownloads[entryIndex];
      topicDownloads = topicDownloads.filter((item) => item);
      return this.storage.set(this.userLoginProvider.user.profile.uid + '_topicDownloads', topicDownloads);
    });
  }

  public checkInternet(): boolean {
    // @TODO on browser network type returns null so lets asume internet its on on browser simulator
    return (this.network.type === null || this.network.type && this.network.type != 'none');
  }


  /**
   *
   */
  public syncVideoDataFromServer(): Promise<any> {
    let after = 1453926411; // init timestamp
    return this.http.get(this.baseAPIPath + DataProvider.VIDEO_SYNC_ENDPOINT + "?after=" + after).toPromise().then((response: any) => {
      this.storage.get(this.userLoginProvider.user.profile.uid + '_videos').then((storedVideos: any) => {
        storedVideos = storedVideos || {};
        if (response && typeof response.videos != 'undefined') {
          for (var i in response.videos) {
            if (!storedVideos[i]) {
              storedVideos[i] = {};
            }
            storedVideos[i].playbackTime = response.videos[i];
            storedVideos[i].timestamp = Math.round(Date.now() / 1000);
            storedVideos[i]._synchronized = true;
          }
          //console.log('stored videos', storedVideos);
          return this.storage.set(this.userLoginProvider.user.profile.uid + '_videos', storedVideos);
        } else {
          throw response.error;
        }
      });
    });
  }

  public syncVideoDataToServer() {
    //console.log('syncVideoDataToServer');
    if (this.checkInternet() && this.userLoginProvider.isLoggedIn()) {
      let body = new URLSearchParams();
      return this.storage.get(this.userLoginProvider.user.profile.uid + '_videos').then((storedVideos: IVideo[]) => {
        let i = 0;
        for (var videoId in storedVideos) {
          if (videoId && videoId != undefined && videoId !== 'undefined') {
            let video = storedVideos[videoId];
            let section = this.findSectionByVideoId(videoId);
            let topic = this.findTopicByVideoId(videoId);
            if (!video._synchronized) {
              body.set("videos[" + i + "][videoId]", String(videoId));
              body.set("videos[" + i + "][progressTime]", String(video.playbackTime));
              body.set("videos[" + i + "][lastWatched]", String(video.timestamp));
              if (section) {
                let entitlementSku = ( section.type == 'regular_course' ) ? section.section : section.section + "-CRAM";
                body.set("videos[" + i + "][entitlementSku]", entitlementSku);
              }
              if (topic) {
                body.set("videos[" + i + "][topicId]", topic.id);
              }
              i++;
            }
          }
        }
        if (i) {
          let options = {
            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
          };
          return this.http.post(this.baseAPIPath + DataProvider.VIDEO_SYNC_ENDPOINT, body.toString(), options).toPromise().then((response) => {
            if (response && response['code'] == 200) {
              //console.log("videos updated - mark as _synchronized");
              return this.markAllVideosAsSynchronized();
            }
          })
        }
      });
    }
  }

  private markAllVideosAsSynchronized() {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_videos').then((storedVideos) => {
      for (var videoId in storedVideos) {
        if (storedVideos[videoId]) {
          storedVideos[videoId]._synchronized = true;

        }
      }
      //console.debug('updating video track list:', storedVideos);
      return this.storage.set(this.userLoginProvider.user.profile.uid + '_videos', storedVideos);
    });
  }


  public getTopicDisplayTitle(topic: ITopic): string {
    const section = this.findSectionByTopicId(topic.id);
    if (topic && section) {
      return section.section + ' ' + topic.slug + ': ' + topic.title;
    }
  }

}
