import { Injectable } from '@angular/core';
import {AlertController} from "ionic-angular";

@Injectable()
export class DataUsageWarningProvider {

  private showWarning = true;

  constructor(private alertController:AlertController){}

  public doShowWarning(){
    if(this.showWarning){
      this.showWarningConfirm()
    }
    this.showWarning = false;
  }

  private showWarningConfirm() {
    const confirm = this.alertController.create({
      cssClass: "data-usage-confirm",
      title: "Data usage",
      message: "We recommend using WiFi to stream video lectures to avoid drawing excessively from your data plan.",
      buttons: [
        {
          text: 'OK',
          handler: () => {}
        }
      ]
    });
    confirm.present();
  }


}
