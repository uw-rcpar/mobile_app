import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";
import {INotification} from "../../interfaces/INotification";
import {UserLoginProvider} from "../user-login/user-login";

@Injectable()
export class NotificationLocalProvider {

  constructor(private storage: Storage, private userLoginProvider:UserLoginProvider) {
  }

  public save(notifications: INotification[]): void {
    this.storage.set(this.userLoginProvider.user.profile.uid+'_notifications', notifications);
  };

  public get(): Promise<INotification[]> {
    return this.storage.get(this.userLoginProvider.user.profile.uid+'_notifications');
  }

}
