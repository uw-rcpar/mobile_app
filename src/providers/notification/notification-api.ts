import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {INotification} from "../../interfaces/INotification";
import {Environment} from "../../environments/env";

@Injectable()
export class NotificationApiProvider {
  private BASE = "user/notifications";

  public get(): Promise<INotification[]> {
    return this.http.get<INotification[]>(Environment.API_ENDPOINT + this.BASE).toPromise();
  }

  constructor(public http: HttpClient) {
  }
}
