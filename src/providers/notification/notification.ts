import {Injectable} from '@angular/core';
import {NotificationApiProvider} from "./notification-api";
import {NotificationLocalProvider} from "./notification-local";
import {INotification} from "../../interfaces/INotification";
import {Subject} from "rxjs/Subject";
import {ReplaySubject} from "rxjs/ReplaySubject";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {UserLoginLocalProvider} from "../user-login/user-login-local";
import {UserLoginProvider} from "../user-login/user-login";


@Injectable()
export class NotificationProvider {

  public notifications$ = new BehaviorSubject<INotification[]>([]);
  public notifications: INotification[] = [];
  public notifications_unread$ = this.notifications$.map((notifications: INotification[]) => notifications.filter((notification) => !notification.status));

  public loadNotifications() {
    let temp_notifications;
    this.notificationLocalProvider.get().then((notifications: INotification[]) => {
      temp_notifications = notifications || [];
      this.notifications$.next(temp_notifications);
      this.notificationApiProvider.get().then((notifications: any) => {
        notifications = notifications.notifications;
        temp_notifications = temp_notifications.concat(
          notifications.filter(
            (notification: INotification) => temp_notifications.findIndex((n: INotification) => n.nid == notification.nid) == -1)
        )
        this.notifications$.next(temp_notifications);
      })
    })
  }

  public delete(notifications: INotification[]) {
    for (let i = 0; i < this.notifications.length; i++) {
      for (let notificationToDelete of notifications) {
        if (this.notifications[i].nid == notificationToDelete.nid) {
          this.notifications[i] = {...this.notifications[i], status: "deleted"}
        }
      }
    }
    this.notifications$.next(this.notifications);
  }

  public markAsRead(notifications: INotification) {
    for (let i = 0; i < this.notifications.length; i++) {
        if (this.notifications[i].nid == notifications.nid) {
          this.notifications[i] = {...this.notifications[i], status: "read"}
        }
    }
    this.notifications$.next(this.notifications);
  }

  constructor(private notificationApiProvider: NotificationApiProvider, private notificationLocalProvider: NotificationLocalProvider) {

  }

  public perform(){
    this.loadNotifications();
    this.notifications$.subscribe((notifications: INotification[]) => {
      this.notifications = notifications;
      this.notificationLocalProvider.save(notifications);
    })
  }

}
