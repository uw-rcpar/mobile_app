import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {UserLoginProvider} from "../user-login/user-login";


@Injectable()
export class VideoDataProvider {

  constructor(public storage: Storage, private userLoginProvider: UserLoginProvider) {
  }

  public updateVideoTime(videoId, playbackTime) {

    let videos;
    //console.log('Update playback time ' + videoId + ' ' + playbackTime);
    this.storage.get(this.userLoginProvider.user.profile.uid + '_videos').then((value) => {
      let videos = value ? value : {};
      videos[videoId] = videos[videoId] || {id: videoId}; // create empty object if not exists
      videos[videoId].playbackTime = Math.round(playbackTime);
      videos[videoId].timestamp = Math.round(Date.now() / 1000);
      videos[videoId]._synchronized = false ;
      //console.log("saving videos", videos);
      this.storage.set(this.userLoginProvider.user.profile.uid + '_videos', videos);
    });
  }

  public loadVideoData(videoId) {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_videos').then((videos) => {
      if (videos) {
        return videos[videoId];
      }
    });
  }


  public loadAllVideoData() {
    return this.storage.get(this.userLoginProvider.user.profile.uid + '_videos');
  }

  public getLastWatchedVideoIdOnSection(sectionSku) {

  }

}
