import { Injectable } from '@angular/core';
import {Subject} from "rxjs/Subject";

@Injectable()
export class MenuProvider {

  change = new Subject<string>();

  disabledMenuSwap(){
    this.change.next("disabled");
  }

  enableMenuSwap(){
   this.change.next("enabled");
  }

}
