import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ToastController} from "ionic-angular";

@Injectable()
export class ToastProvider {

  constructor(private toastController: ToastController) {
  }

  public show(message: string): void {
    this.presentToast(message);

  }

  presentToast(message: string) {
    let toast = this.toastController.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }


}
