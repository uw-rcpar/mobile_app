import {Injectable} from '@angular/core';
import {Platform, ToastController} from "ionic-angular";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Network} from "@ionic-native/network";
import {Environment} from "../../environments/env";
import {ToastProvider} from "../toast/toast";

@Injectable()
export class PlatformOnlyProvider {

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private screenOrientation: ScreenOrientation,
              private network: Network,
              private toastProvider: ToastProvider) {
    if (Environment.RUNNING_ON_PLATFORM) {
      this.platform.ready().then(() => {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
        this.lockDeviceInPortrait();
        this.showNetworkChanges();

      })
    }
  }


  private showNetworkChanges() {
    this.network.onchange().subscribe(() => {
      this.toastProvider.show('Network changed: ' + this.network.type);
    });
  }


  /**
   * Utility method to ensure device is fixed vertical
   */
  private lockDeviceInPortrait() {
    if (this.platform.is('mobileweb') || this.platform.is('core')) {
      //desktop browser only code
      //console.log('Platform: web/browser');
    } else {
      // device-specific code, such as detecting screen rotation
      //console.log('Platform: device');
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
  }
}
