import {Injectable} from '@angular/core';
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Environment} from "../../environments/env";
import {Observable} from "rxjs/Observable";

@Injectable()
export class OrientationProvider {

  public change: Observable<any>;

  constructor(private screenOrientation: ScreenOrientation) {
    this.change = this.screenOrientation.onChange();
    this.change.subscribe((x) => {
      //console.log(x);
    });
  }

  setVerticalLock() {
    return this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  setHorizontalLock() {
    return this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  setVerticalUnLock() {
    //console.log('setVerticalUnLock');
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.screenOrientation.unlock();
  }

  setHorizontalUnLock() {
    //console.log('setHorizontalUnLock');
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
    this.screenOrientation.unlock();
  }

  isVertical() {
    return this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.PORTRAIT || this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY || this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.PORTRAIT_SECONDARY ;
  }

  isHorizontal() {
    return this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.LANDSCAPE || this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.LANDSCAPE_PRIMARY || this.screenOrientation.type == this.screenOrientation.ORIENTATIONS.LANDSCAPE_SECONDARY;
  }

  unlock() {
    return this.screenOrientation.unlock();
  }


}
