import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {DataProvider} from "../data/data";
import {UserLoginProvider} from "../user-login/user-login";
import {Storage} from "@ionic/storage";
import {FileManagerProvider} from "../file-manager/file-manager";
import {LoadingController, Loading} from "ionic-angular";

@Injectable()
export class BootProvider {

  private loader: Loading;

  constructor(private http: HttpClient,
              private data: DataProvider,
              private storage: Storage,
              private userLoginProvider: UserLoginProvider,
              private fileManager: FileManagerProvider,
              private loadingCtrl: LoadingController
  ) {
  }


  /**
   * Check storage buckets [user, entitlements, sections, topicData]. Ensure each is created or create it.
   */
  public checkStorage(): Promise<any> {
    //console.log('initSchema');
    // Set the initial default value foreach collection
    const storageBuckets = {
      'uiState': {},
      'videos': {},
      'topicDownloads': [],
      'user': {},
      'hasLoggedIn': false,
      'currentSection': null,
      'courseOutline': null
    };

    let storageReady = true;
    let promises = [];

    // Build primises list on each storage bucket
    Object.keys(storageBuckets).forEach((key) => {
      //console.log('checking bucket ' + key);
      let promise = this.storage.get(this.userLoginProvider.user.profile.uid + '_' + key).then((value) => {
        if (value === null) {
          //console.log('Bucket not exists.. setting default value for ' + key);
          return this.storage.set(this.userLoginProvider.user.profile.uid + '_' + key, storageBuckets[key]);
        }
        return value;
      });
      promises.push(promise);
    });
    return Promise.all(promises);
  }


  private initFilesystem(): Promise<void> {
    return this.fileManager.createDirectoryStructure();
  }

  /**
   * Requires internet connection
   */
  public sync(): Promise<any> {
    let syncSteps = [
      this.data.syncCourseOutline(),
      this.data.syncVideoDataFromServer()
    ];
    return Promise.all(syncSteps).catch((error) => {
      console.log("Failed syncronization - networking issue ?");
    });
  }


  /**
   * Create a new loader, and show the message
   *
   * @param {string} message
   */
  loaderCreateAndPresent(message: string) {
    this.loader = this.loadingCtrl.create({
      content: message,
      duration: 3000
    });
    this.loader.present();
  }

  /**
   * Close the loading dialog
   */
  loaderDismiss() {
    this.loader.dismiss();
    this.loader = null;
  }


  public boot(): Promise<any> {
    this.loaderCreateAndPresent("Initializing..");
    if (this.userLoginProvider.isLoggedIn()) {
      return this.checkStorage().then(() => {
        this.loader.setContent("Checking Entitlements");
        this.userLoginProvider.validateEntitlements();
        this.loader.setContent("Checking Connection");
        this.loader.setContent("Performing online syncronization");
        return this.sync().then(() => {
          return this.loaderDismiss();
        })
      })
    } else {
      this.loaderDismiss();
      throw 'not_logged_in';
    }
  }
}
