import {Injectable} from "@angular/core";
import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {UserLoginProvider} from "../providers/user-login/user-login";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  shouldInterceptResponse(event): boolean {
    return this.userLogin.isLoggedIn();
  }

  interceptResponse(event) {
    console.log('## interceptor Response !', event);
    if (event.body && event.body['entitlementNeedsRefresh']) {
      this.userLogin.refreshToken();
    }

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.userLogin.isLoggedIn()) {
      console.log("# interceptor request to " + req.url + " using token")
      const authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + this.userLogin.user.token).set('JWT', this.userLogin.user.token)
      });
      return next.handle(authReq).do((e) => {
        //console.log(e);
        if (e instanceof HttpResponse && this.shouldInterceptResponse(e)) {
          this.interceptResponse(e);
        }
        //return e;
      });
    } else {
      //console.log("request to " + req.url + " without token")
      return next.handle(req);
    }
  }

  constructor(public userLogin: UserLoginProvider) {

  }
}

export const AuthInterceptorProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
}
