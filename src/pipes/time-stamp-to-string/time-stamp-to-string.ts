import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeStampToString',
})
export class TimeStampToStringPipe implements PipeTransform {
  monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  transform(value: string, ...args) {
    const date = new Date(Number(value) * 1000);
    const datevalues = this.monthNames[date.getMonth()] + " " +date.getDay();
    return datevalues;
  }
}
