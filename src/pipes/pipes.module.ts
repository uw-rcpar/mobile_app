import {NgModule} from '@angular/core';
import {HoursMinutesSecondsPipe} from './hours-minutes-seconds/hours-minutes-seconds';
import {FilterPipe} from "./filter/filter";
import { TimeStampToStringPipe } from './time-stamp-to-string/time-stamp-to-string';


@NgModule({
  declarations: [HoursMinutesSecondsPipe, FilterPipe,
    TimeStampToStringPipe],
  imports: [],
  exports: [HoursMinutesSecondsPipe, FilterPipe,
    TimeStampToStringPipe
  ]
})
export class PipesModule {
}
