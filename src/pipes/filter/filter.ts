import {Pipe, PipeTransform} from '@angular/core';
/**
 * Generated class for the FilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(values: any, cond: any, ...args) {
    if (!values) return [];
    let fields =  Object.keys(cond);
    if (!fields) return [];
    let field = fields[0];
    let value = cond[field];
    return values.filter(it => it[field] == value);
  }
}
