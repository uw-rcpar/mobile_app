import { Pipe, PipeTransform } from '@angular/core';

/**
 * Given a decimal value, like 10.25 - convert to hour:min:seconds, like 10:15
 */
@Pipe({
  name: 'hoursMinutesSeconds',
})
export class HoursMinutesSecondsPipe implements PipeTransform {

  /**
   * Takes a decimal and makes it HH:MM:SS
   */
  transform(value: number, ...args) {
    const minutes: number = Math.floor(value / 60) || 0;
    const hours: number = Math.floor(minutes / 60) || 0;
    const seconds: number = Math.floor(value % 60) || 0;

    let output = this.pad(minutes) + ':' + this.pad(seconds);

    // only prepend hours if there is a positive value for this number
    if (hours > 0) {
      output = this.pad(hours) + ':' + output;
    }

    return output;
  }

  /**
   * Custom little pad function to convert a number to a string with padding
   *
   * @param {number} value
   * @returns {any}
   */
  private pad(value:number) {
    return (value < 10 ? '0' : '') + value;
  }
}
