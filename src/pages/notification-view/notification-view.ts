import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {INotification} from "../../interfaces/INotification";
import {NotificationProvider} from "../../providers/notification/notification";

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

@Component({
  selector: 'page-notification-view',
  templateUrl: 'notification-view.html',
})
export class NotificationViewPage {
  public notification: INotification;

  constructor(public navParams: NavParams, notificationProvider: NotificationProvider) {
    this.notification = this.navParams.get("notification");
    notificationProvider.markAsRead(this.notification);
  }

  get stringdate(){
    if(this.notification){
      const date = new Date(Number(this.notification.created_date) * 1000);
      const datevalues = monthNames[date.getMonth()] + " " +date.getDay()+ ", " +date.getFullYear();
      return datevalues;
    }
  }

}
