import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavParams, NavController} from "ionic-angular";
import {OrientationProvider} from "../../providers/orientation/orientation";
import {VideosPage} from "../videos/videos";
import {DownloadsPage} from "../downloads/downloads";

@Component({
  selector: 'page-pdf',
  templateUrl: 'pdf.html',
})
export class PdfPage implements OnInit, OnDestroy{
  public displayTitle: string;
  public pdfUrl: string;
  public pdfPath: string;
  public pdfFile: string;
  public offline: boolean;

  public course_type: string;
  private referer: string;


  constructor(public navParams: NavParams,
              private navCtrl: NavController,
              public orientationProvider:OrientationProvider) {
  }

  ngOnInit(): void {
    this.displayTitle = this.navParams.get("displayTitle")
    this.pdfUrl = this.navParams.get('url');
    this.pdfPath = this.navParams.get('path');
    this.pdfFile = this.navParams.get('localFile');
    this.offline = this.navParams.get('offline');
    this.course_type = this.navParams.get('course_type');
    this.referer = this.navParams.get('referer');
    this.orientationProvider.unlock()
  }

  ngOnDestroy(): void {
    this.orientationProvider.setVerticalLock();
  }


  customBack() {
    //console.log('back', this.referer, {type: this.course_type});
    if (this.referer == "VideosPage") {
      this.navCtrl.setRoot(VideosPage, {type: this.course_type});
    }else if(this.referer == "DownloadsPage") {
      this.navCtrl.setRoot(DownloadsPage);
    }
  }


}
