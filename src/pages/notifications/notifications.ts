import {Component} from '@angular/core';
import {NotificationProvider} from "../../providers/notification/notification";
import {INotification} from "../../interfaces/INotification";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/filter";
import {NotificationViewPage} from "../notification-view/notification-view";
import {NavController, NavParams} from "ionic-angular";
import {NetworkProvider} from "../../providers/network/network";
import {DataProvider} from "../../providers/data/data";

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  public editable = false;
  public selected_notifications = [];
  public notifications$: Observable<INotification[]>;
  public sectionName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public notificationProvider: NotificationProvider, private networkProvider: NetworkProvider, private dataProvider: DataProvider) {
    this.notifications$ = this.notificationProvider.notifications$.map((notifications: INotification[]) => notifications.filter((notification) => notification.status != "deleted"));
    this.notifications$.subscribe(x => console.log(x))
    this.dataProvider.loadCurrentSectionSku().then((section) => this.sectionName = section);
  }

  public toggleEditable() {
    this.editable = !this.editable;
  }

  public addOrRemoveSelectedOption(notification: INotification) {
    let notificationIndex = this.selectedNotificationsContainsNotification(notification);
    if (notificationIndex != -1) {
      this.selected_notifications = this.selected_notifications.filter((n: INotification) => n.nid != notification.nid)
    } else {
      this.selected_notifications.push(notification);
    }
  }

  public selectedNotificationsContainsNotification(notification: INotification): number {
    return this.selected_notifications.findIndex((n: INotification) => n && n.nid == notification.nid)
  }

  public saveDeleted() {
    this.notificationProvider.delete(this.selected_notifications)
  }

  public goToNotificationView(notification: INotification) {
    this.navCtrl.push(NotificationViewPage, {notification: notification});
  }

}
