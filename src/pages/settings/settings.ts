import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, App, AlertController} from 'ionic-angular';
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {LoginPage} from "../login/login";
import {IUser} from '../../interfaces/IUser';
import {TourPage} from "../tour/tour";
import {NetworkProvider} from "../../providers/network/network";
import {ISection} from "../../interfaces/ISection";
import {DataProvider} from "../../providers/data/data";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  public sections: ISection[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private app: App,
              private userLogin: UserLoginProvider,
              private networkProvider: NetworkProvider,
              private dataProvider: DataProvider,
              private alertCtrl: AlertController
  ) {

    this.dataProvider.loadCurrentSectionSku().then((currentSectionSku: string) => {
      this.sections = [
        this.dataProvider.findSectionBySkuAndType(currentSectionSku, 'regular_course'),
        this.dataProvider.findSectionBySkuAndType(currentSectionSku, 'cram_course')
      ];
    });
  }


  showLogoutConfirm() {
    const confirm = this.alertCtrl.create({
      cssClass: "logout-confirm",
      title: "Are you sure you want to log out?",
      message: "You won't be able to access downloaded content while offline unless you're already logged in.",
      buttons: [
        {
          text: 'Stay logged in',
          handler: () => {}
        },
        {
          text: 'Log out',
          handler: () => {
            this.userLogin.logout();
          }
        }
      ]
    });
    confirm.present();
  }


  launchIntro() {
    this.navCtrl.push(TourPage)
  }

}
