import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {DataProvider} from "../../providers/data/data";
import {MenuController} from 'ionic-angular';
import {OrientationProvider} from "../../providers/orientation/orientation";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public downloads: any = [];

  constructor(public navCtrl: NavController,
              private navParams: NavParams,
              private userLoginProvider: UserLoginProvider,
              private dataProvider: DataProvider,
              public menuCtrl: MenuController,
              private orientationProvider: OrientationProvider
  ) {


  }

  ionViewDidEnter(): void {
    this.orientationProvider.setVerticalLock();
  }


  ionViewWillEnter() {
    // if we are on this page, we should assume the user does not have a current section (nav params should drive this.)
    // OR we could use the database to auto-navigate to the correct section for the user
    // this.dataProvider.loadCurrentSectionSku().then((section) => {
    //   // this.currentSection = section;
    //   console.log('Home.section...', section.title);
    // });
    this.menuCtrl.open();
  }

}
