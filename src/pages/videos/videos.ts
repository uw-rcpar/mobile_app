import {Component, OnDestroy, OnInit} from '@angular/core';
import {Platform, NavController, NavParams, AlertController, ActionSheetController} from 'ionic-angular';
import {DataProvider} from '../../providers/data/data' ;
import {VideoDataProvider} from "../../providers/video-data/video-data";
import {ViewChild, ElementRef} from '@angular/core';
import {PlayerComponent} from "../../components/player/player";
import {FileTransfer, FileTransferObject} from '@ionic-native/file-transfer';
import {File, FileEntry} from '@ionic-native/file';
import {PdfPage} from "../pdf/pdf";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {IPlayerStatus} from "../../interfaces/IPlayerStatus";
import {LoadingController} from "ionic-angular";
import {ITopic} from '../../interfaces/ITopic';
import {Loading} from 'ionic-angular';
import {FileManagerProvider} from "../../providers/file-manager/file-manager";
import {ISection} from "../../interfaces/ISection";
import {OrientationProvider} from "../../providers/orientation/orientation";
import {Environment} from "../../environments/env";
import {IChapter} from "../../interfaces/IChapter";
import {MenuProvider} from "../../providers/menu/menu";
import {IDownload} from "../../interfaces/IDownload";
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {ExternalLinksProvider} from "../../providers/external-links/external-links";
import {NetworkProvider} from "../../providers/network/network";
import {DataUsageWarningProvider} from "../../providers/data-usage-warning/data-usage-warning";

@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage implements OnInit, OnDestroy {

  @ViewChild('appPlayerComponent') appPlayerComponent: PlayerComponent;
  @ViewChild('videoList') videoList: ElementRef;

  loader: Loading;
  section: ISection;
  videoData = {};
  currentTopic: ITopic;
  currentChapter: IChapter;
  courseType: string;
  localVideoUrl: string = "";
  playMode: string = "";
  fullscreen: boolean = false;
  downloads: Array<any>;
  expandedChapterIds: Array<string> = [];
  isPlaying: boolean;
  allowedDownloads: boolean;
  autoplay: boolean = false;
  hasRestrictedTopics: boolean;
  topicRestrictions: Array<string>;
  apiBaseUrl: string;


  constructor(private loadingCtrl: LoadingController,
              private dataProvider: DataProvider,
              private videoDataProvider: VideoDataProvider,
              private navCtrl: NavController,
              private navParams: NavParams,
              private transfer: FileTransfer,
              private actionSheetCtrl: ActionSheetController,
              private file: File,
              private userLoginProvider: UserLoginProvider,
              private fileManager: FileManagerProvider,
              private alertCtrl: AlertController,
              private orientation: OrientationProvider,
              private externalLinksProvider: ExternalLinksProvider,
              public menuProvider: MenuProvider,
              private networkProvider: NetworkProvider,
              private dataUsageWarningProvider: DataUsageWarningProvider) {


    this.apiBaseUrl = Environment.API_BASE_URL;
    this.dataProvider.getUserAllowedSections();
    this.orientation.change.subscribe(() => {
      this.fullscreen = this.orientation.isHorizontal();
    });

    this.userLoginProvider.hasOfflineAccessPermissions(this.section);
  }

  /**
   * Listener to fire when a player goes full screen
   *
   * @param $event:IPlayerStatus
   */
  onFullScreen($event: IPlayerStatus) {
    // Hack ! only setHorizontalLock does not work after rotating left
    if ($event && $event.isFullscreen) {
      this.orientation.setVerticalLock().then(() => {
        this.orientation.setHorizontalLock();
      });
    } else {
      // Hack ! only setVerticalLock does not work after rotating first time
      // possible bun on screen orientation plugin
      this.orientation.setHorizontalLock().then(() => {
        this.orientation.setVerticalLock();
      });
    }
  }

  pause() {
    this.isPlaying = false;
    this.appPlayerComponent.pause();
  }

  play(topic: ITopic, autoplay?) {
    //console.log("play video!")
    this.isPlaying = true;
    if (!(this.hasRestrictedTopics && this.topicRestrictions.indexOf(topic.id) == -1)) {
      if (this.appPlayerComponent) {
        this.fullscreen = this.appPlayerComponent.fullscreen;
      }
      this.dataProvider.setCurrentVideoId(this.section, topic.video_id);
      this.currentTopic = topic;
      this.localVideoUrl = null;
      this.playMode = 'online';
      if (this.appPlayerComponent) {
        if (this.isDownloaded(topic)) {
          let download = this.getDownload(topic);
          let localFile = this.fileManager.getUrlFilename(download.pdf.localUrl);
          let path = this.fileManager.getUrlPath(download.pdf.localUrl);
          this.appPlayerComponent.loadPdfData({offline: true, path: path, localFile: localFile});
          this.playMode = "offline";
          this.localVideoUrl = download.video.localUrl;
        } else {
          this.playMode = "online";
          this.appPlayerComponent.loadPdfData({offline: false, url: topic.pdf_url});
        }
        //console.log('playMode', this.playMode);
      }
      this.appPlayerComponent.start(topic.video_id, !autoplay);
    }
  }

  getTopicDisplayTitle(section: ISection, topic: ITopic): string {
    if (topic && section) {
      return section.section + ' ' + topic.slug + ': ' + topic.title;
    }
  }

  viewPDF(topic) {
    if (!(this.hasRestrictedTopics && this.topicRestrictions.indexOf(topic.id) == -1)) {
      let displayTitle: string = this.getTopicDisplayTitle(this.section, topic);
      if (this.isDownloaded(topic)) {
        let download = this.getDownload(topic);
        let localFile = this.fileManager.getUrlFilename(download.pdf.localUrl);
        let path = this.fileManager.getUrlPath(download.pdf.localUrl);
        this.navCtrl.setRoot(PdfPage, {
          offline: true,
          path: path,
          localFile: localFile,
          displayTitle: displayTitle,
          course_type: this.courseType,
          referer: "VideosPage"
        });
      } else {
        this.navCtrl.setRoot(PdfPage, {
          offline: false,
          url: topic.pdf_url,
          displayTitle: displayTitle,
          course_type: this.courseType,
          referer: "VideosPage"
        });
      }
    }
  }

  private onTopicDownloadEnded(topic, success) {
    //console.log('onTopicDownloadEnded');
    this.fileManager.downloadingTopics[topic.id] = false;
    if (success) {
      this.fileManager.downloadedTopics[topic.id] = true;
    }
    this.refreshDownloads();
  }

  /**
   * Handle click action to fetch a file
   *
   * @param {ITopic} topic
   */
  private download(topic: ITopic) {
    if (!(this.hasRestrictedTopics && this.topicRestrictions.indexOf(topic.id) == -1)) {
      this.fileManager.downloadingTopics[topic.id] = true;
      this.downloadPDF(topic).then(() => {
        return this.downloadVideo(topic).then(() => {
          this.onTopicDownloadEnded(topic, true);
        }).catch((error) => {
          this.onTopicDownloadEnded(topic, false);
        });
      }).catch((error) => {
        this.onTopicDownloadEnded(topic, false);
      });
    }
  }

  private downloadPDF(topic: ITopic) {
    //console.log('downloadPDF', topic.title);
    let localPDFUrl: String = '';
    const fileTransfer: FileTransferObject = this.transfer.create();
    const url = topic.pdf_url;
    const localFile = this.fileManager.buildTopicFilePath(this.section.section, this.section.type, topic.id, 'pdf');
    const downloadTitle = this.buildDownloadTitle(this.section.section, topic);
    // Pdf ebook specific download metadata
    let pdfEntry = {
      localFilename: localFile,
      localUrl: localPDFUrl,
      url: url,
      status: DataProvider.TOPIC_DOWNLOAD_STATUS_PENDING
    };
    return this.dataProvider.saveDownload(topic.id, 'pdf', downloadTitle, this.section.section, this.section.type, null, pdfEntry).then(() => {
      return fileTransfer.download(url, localFile, true).then((entry) => {
        pdfEntry.localUrl = entry.toURL();
        pdfEntry.status = DataProvider.TOPIC_DOWNLOAD_STATUS_DOWNLOADED;
        return this.dataProvider.saveDownload(topic.id, 'pdf', downloadTitle, this.section.section, this.section.type, null, pdfEntry);
      }).catch((error) => {
        this.onTopicDownloadFail(topic);
      });
    });

  }

  /**
   * Creates and present an alert message when video download failes
   */
  private showTopicDownloadError() {
    let alert = this.alertCtrl.create({
      title: 'Topic Download Failed',
      subTitle: 'Failed to download the topic',
      buttons: ['OK']
    });
    alert.present();
  }


  /**
   * Called when a video download fails fot the given topic
   * @param topic
   */
  private onTopicDownloadFail(topic: ITopic) {
    this.removeTopicDownload(topic);
    this.showTopicDownloadError();
  }


  private downloadVideo(topic: ITopic) {
    //console.log('downloadVideo topic', topic);
    let localVideoUrl: string = '';
    const fileTransfer: FileTransferObject = this.transfer.create();
    if (topic.video_urls) {
      const url = topic.video_urls.high.url;
      const type = 'video/mp4';
      const localFile = this.fileManager.buildTopicFilePath(this.section.section, this.section.type, topic.id, 'mp4');
      const downloadTitle = this.buildDownloadTitle(this.section.section, topic);
      let videoEntry = {
        localFilename: localFile,
        url: url,
        videoId: topic.video_id,
        status: DataProvider.TOPIC_DOWNLOAD_STATUS_PENDING,
        localUrl: localVideoUrl,
        type: type
      };

      return this.dataProvider.saveDownload(topic.id, 'video', downloadTitle, this.section.section, this.courseType, videoEntry, null).then(() => {
        return fileTransfer.download(url, localFile, true, {headers: {"Range": "bytes=0-"}}).then((entry: FileEntry) => {
          videoEntry.status = DataProvider.TOPIC_DOWNLOAD_STATUS_DOWNLOADED;
          videoEntry.localUrl = entry.toURL();
          return this.dataProvider.saveDownload(topic.id, 'video', downloadTitle, this.section.section, this.courseType, videoEntry, null);
        }).catch((error) => {
          this.onTopicDownloadFail(topic);
        });
      });

    }
  }

  refreshDownloads(): Promise<any> {
    if (this.section) {
      return this.dataProvider.getTopicDownloads(this.section.section).then((downloads) => {
        this.downloads = downloads;
        return downloads;
      });
    }
    return Promise.reject(null);
  }

  isDownloaded(topic): boolean {
    return this.fileManager.downloadedTopics[topic.id];
  }

  getDownload(topic): IDownload {
    if (this.downloads) {
      return this.downloads.find((item) => item.topicId == topic.id);
    }
    return null;
  }

  presentDeleteConfirm(topic: ITopic) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to remove this download ? You will no longer be able to access it offline. ',
      buttons: [{
        cssClass: "remove",
        text: 'Remove Download',
        handler: () => {
          this.removeTopicDownload(topic);
        }
      }, {
        cssClass: "cancel",
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
        }
      }
      ]
    });
    actionSheet.present();
  }

  removeTopicDownload(topic) {
    this.fileManager.removeDownload(topic.id).then(() => {
      return this.refreshDownloads();
    }).catch((error) => {
      return this.refreshDownloads();
    })
  }

  ngOnInit(): void {
    this.dataUsageWarningProvider.doShowWarning();
    this.fileManager.init()
    this.orientation.setVerticalLock();
    this.videoDataProvider.loadAllVideoData().then((videoData) => {
      this.videoData = videoData;
    });

    this.dataProvider.loadCurrentSectionSku().then((currentSectionSku: string) => {
      this.courseType = this.navParams.get('type'); // cram / review
      this.autoplay = this.navParams.get('autoplay');
      this.section = this.dataProvider.findSectionBySkuAndType(currentSectionSku, this.courseType);
      this.allowedDownloads = this.userLoginProvider.hasOfflineAccessPermissions(this.section);
      this.hasRestrictedTopics = this.userLoginProvider.hasRestrictedTopics(this.section);
      this.topicRestrictions = this.userLoginProvider.getTopicRestrictions(this.section);
      //console.log(this.hasRestrictedTopics, this.topicRestrictions);
      this.dataProvider.loadCurrentTopic(this.section, true).then((currentTopic) => {
        // read Current topic from nav parameters - read from disk if not set
        if (this.navParams.get('currentTopic')) {
          this.currentTopic = this.navParams.get('currentTopic');
          //console.log('setting current topic from parameters', this.currentTopic);
        } else {
          //console.log('setting current topic from db');
          this.currentTopic = currentTopic;
        }
        return this.currentTopic;
      }).then((currentTopic: ITopic) => {
        //console.log(currentTopic)
        if (currentTopic) {
          if (this.autoplay) {
            setTimeout(() => {
              this.play(currentTopic, true);
            }, 1);
          }
          this.currentChapter = this.dataProvider.findChapterByTopicId(currentTopic.id);
          this.expandChapter(this.currentChapter.id);
        }
      });

      return currentSectionSku;
    }).then(() => {
      this.refreshDownloads();
    })
  }

  ngOnDestroy(): void {
    if (this.appPlayerComponent) {
      this.appPlayerComponent.pause();
    }
  }

  ionViewDidLeave(): void {
    this.dataProvider.syncVideoDataToServer();
  }


  ionViewWillEnter() {
    this.fileManager.init();
  }

  getPageTitle(section: ISection) {
    if (section) {
      if (section.type == 'regular_course') {
        return 'Review Course'
      } else {
        return 'Cram Course';
      }
    }
  }

  isChapterExpanded(chapterId: string): boolean {
    return this.expandedChapterIds.indexOf(chapterId) != -1;
  }

  toggleExpandedChapter(chapterId: string) {
    if (this.isChapterExpanded(chapterId)) {
      this.collapseChapter(chapterId);
    } else {
      this.expandChapter(chapterId);
    }
  }

  expandChapter(chapterId: string) {
    this.expandedChapterIds.push(chapterId);
  }

  collapseChapter(chapterId: string) {
    let index = this.expandedChapterIds.indexOf(chapterId);
    delete this.expandedChapterIds[index];
  }

  buildDownloadTitle(sectionId: string, topic: ITopic) {
    return sectionId + " " + topic.slug + " : " + topic.title;
  }

  ionViewWillLeave() {
    this.menuProvider.enableMenuSwap();
  }

  public isPlayingChange(status) {
    //console.log('isPlaying Status change', status);
    this.isPlaying = status;
    this.dataProvider.syncVideoDataToServer();
  }

  public videoRowClick(topic, event) {
    if (this.hasRestrictedTopics && this.topicRestrictions.indexOf(topic.id) == -1) {
      event.preventDefault();
      let alert = this.alertCtrl.create({
        cssClass: "restricted-topic",
        title: 'Currently Unavailable',
        message: 'You don\'t have access to this content.',
        buttons: ['OK']
      });
      alert.present();
      //let url = this.apiBaseUrl + '/mb/restricted' + "?section=" + this.section.section + "&uid=" + this.userLoginProvider.user.profile.uid;
      //this.externalLinksProvider.open(url);
      return false;
    } else {
      return true;
    }

  }

}
