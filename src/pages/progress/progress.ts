import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {UserLoginProvider} from "../../providers/user-login/user-login";
import 'rxjs/add/observable/fromPromise';
import {DataProvider} from "../../providers/data/data";
import {VideosPage} from "../videos/videos";
import {ISection} from "../../interfaces/ISection";
import {IUser} from "../../interfaces/IUser";
import {ProgressProvider} from "../../providers/progress/progress";
import {ICourseOutline} from "../../interfaces/ICourseOutline";
import {VideoDataProvider} from "../../providers/video-data/video-data";
import * as HighCharts from 'highcharts';
import {ExternalLinksProvider} from "../../providers/external-links/external-links";
import {IProgressPercent} from "../../interfaces/IProgressPercent";
import {NetworkProvider} from "../../providers/network/network";
import {ILastWatched} from "../../interfaces/ILastWatched";
declare var require: any;
require('highcharts/highcharts-more')(HighCharts);
require('highcharts/modules/solid-gauge')(HighCharts);

@Component({
  selector: 'page-progress',
  templateUrl: 'progress.html'
})
export class ProgressPage {
  private courseOutline: ICourseOutline;
  private currentSectionSku: string;
  public currentTopic: any;
  public showContinueWatching: boolean;
  public downloads: any = [];
  public sections: ISection[];
  private progress: {
    "regular_course": number,
    "cram_course": number
  };

  constructor(public navCtrl: NavController,
              private userLoginProvider: UserLoginProvider,
              private dataProvider: DataProvider,
              private videoDataProvider: VideoDataProvider,
              public externalLinksProvider: ExternalLinksProvider,
              private userProvider: UserLoginProvider,
              private progressProvider: ProgressProvider,
              private networkProvider: NetworkProvider) {

    this.progress = {
      "regular_course": 0,
      "cram_course": 0
    };
  }

  readLastWatchedFromServerResponse(serverProgressResponse: any, sku: string): ILastWatched {
    if (serverProgressResponse.last_watched) {
      return serverProgressResponse.last_watched.find((el: ILastWatched) => el.sku == sku);
    }
    return null ;
  }


  buildProgressData(serverProgressResponse) {
    if (serverProgressResponse) {
      let progressPercents: IProgressPercent[] = serverProgressResponse.progress_percentages;
      if (progressPercents) {
        let regularCoursePercent: IProgressPercent = progressPercents.find((p) => p.sku == this.currentSectionSku);
        let cramCoursePercent: IProgressPercent = progressPercents.find((p) => p.sku == this.currentSectionSku + '-CRAM');
        this.progress.regular_course = regularCoursePercent ? regularCoursePercent.percentage : 0;
        this.progress.cram_course = cramCoursePercent ? cramCoursePercent.percentage : 0;
      }
    }
  }

  /**
   * TODO Check the best way to share section data across pages
   */
  ionViewDidEnter() {
    Promise.all([
      this.dataProvider.getCourseOutline(),
      this.dataProvider.loadCurrentSectionSku(),
      this.videoDataProvider.loadAllVideoData(),

    ]).then((values: Array<any>) => {
      // Read Parameters
      this.courseOutline = values[0];
      this.currentSectionSku = values[1];
      let videoData = values[2];

      // Display / Hide continue watching block based on permissions
      this.showContinueWatching = this.hasPermissions("cram_course") || this.hasPermissions("regular_course");

      // Load section list (AUD, AUD-CRAM) - requrid fot no-network provider arguments
      this.sections = [
        this.dataProvider.findSectionBySkuAndType(this.currentSectionSku, 'regular_course'),
        this.dataProvider.findSectionBySkuAndType(this.currentSectionSku, 'cram_course')
      ];

      // Calculates the progress for each section
      return this.progressProvider.fetchSectionProgress([this.currentSectionSku, this.currentSectionSku + '-CRAM']);
    }).then((serverProgressResponse: any) => {
      this.buildProgressData(serverProgressResponse);
      let lastWatched: ILastWatched = this.readLastWatchedFromServerResponse(serverProgressResponse, this.currentSectionSku);
      if (lastWatched) {
        //console.log("Last watched topic id from API = " + lastWatched.topic_id) ;
        this.currentTopic = this.dataProvider.findTopicById(lastWatched.topic_id);
        //console.log("current Topic", this.currentTopic);
      }else{
        //console.log('unable to read last watched from API');
      }
      this.renderCharts();

    });
  }

  getChartOptions(type, percent) {
    //http://jsfiddle.net/gh/get/library/pure/highcharts/highcharts/tree/master/samples/highcharts/demo/gauge-activity/
    return {
      'chart': {
        'renderTo': type + '_chart',
        'type': 'solidgauge',
        'style': {
          'fontFamily': '-apple-system", "Helvetica Neue", "Roboto", sans-serif;'
        },
        'height': 200
      },
      'title': null,
      'color': '#57caf3',
      'credits': false,
      'tooltip': {
        'enabled': false
      },
      'pane': {
        'center': ['50%', '50%'],
        'size': '200px',
        'startAngle': 0,
        'endAngle': 360,
        'background': {
          'backgroundColor': '#d0d2d2',
          'innerRadius': '50%',
          'outerRadius': '60%',
          'borderWidth': 0
        }
      },
      'yAxis': {
        'min': 0,
        'max': 100,
        'labels': {
          'enabled': false
        },
        'lineWidth': 0,
        'minorTickInterval': null,
        'tickPixelInterval': 400,
        'tickWidth': 0,
        'title': {
          'text': 'Lectures watched',
          'y': 135,
          'style': {
            'color': '#6c6b6b',
            'fontSize': '18px'
          }
        }
      },
      'plotOptions': {
        'solidgauge': {
          'innerRadius': '50%',
          'radius': '60%',
          'dataLabels': {
            'y': -20,
            'borderWidth': 0,
            'useHTML': true
          }
        }
      },
      'series': [{
        'name': '',
        'data': [percent],
        'dataLabels': {
          'format': '<div style="text-align:center; color: #0a3334; font-size:25px;">{y}%</div>'
        },
      }]
    };
  }

  renderCharts() {
    // Create the chart
    if (document.getElementById('regular_course_chart')) {
      HighCharts.chart(this.getChartOptions('regular_course', this.progress.regular_course));
    }

    if (document.getElementById('cram_course_chart')) {
      HighCharts.chart(this.getChartOptions('cram_course', this.progress.cram_course));
    }
  }

  /**
   * Callback to handle click for continue watching
   */
  continueWatching() {
    let section: ISection = this.dataProvider.findSectionByVideoId(this.currentTopic.video_id);
    if (section) {
      this.navCtrl.setRoot(VideosPage, {type: section.type, autoplay: true, currentTopic: this.currentTopic});
    }
  }

  viewOutline(type) {
    this.navCtrl.setRoot(VideosPage, {type: type});
  }


  showCourse(courseType) {
    return this.hasPermissions && this.isActivated(courseType);
  }

  hasPermissions(courseType) {
    let entitlements = this.userLoginProvider.user.entitlements;
    let section = this.currentSectionSku;
    if (section) {
      let suffix = '';
      if (courseType == 'cram_course') {
        suffix = '-CRAM';
      }
      let sku = section + suffix;
      let index = entitlements.findIndex((en) => en.sku == sku);
      return index != -1;
    }
    return false;
  }


  isActivated(courseType) {
    let entitlements = this.userLoginProvider.user.entitlements;
    let section = this.currentSectionSku;
    if (section) {
      let suffix = '';
      if (courseType == 'cram_course') {
        suffix = '-CRAM';
      }
      let sku = section + suffix;
      let entitlement = entitlements.find((en) => en.sku == sku);
      return entitlement && entitlement.activated ;
    }
    return false;
  }


  gotoActivateCourse(courseId) {
    let section = this.currentSectionSku;
    let url = this.userProvider.getInterruptPageUrl(section, courseId + "-activate");
    this.externalLinksProvider.open(url);
  }


}
