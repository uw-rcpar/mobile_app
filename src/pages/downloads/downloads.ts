import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, ActionSheet, ActionSheetController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {PdfPage} from "../pdf/pdf";
import {File} from '@ionic-native/file';
import {FileManagerProvider} from "../../providers/file-manager/file-manager";
import {IDownload} from "../../interfaces/IDownload";
import {OrientationProvider} from "../../providers/orientation/orientation";
import {IPlayerStatus} from "../../interfaces/IPlayerStatus";
import {PlayerComponent} from "../../components/player/player";
import {Environment} from "../../environments/env";

/**
 * Generated class for the DownloadsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-downloads',
  templateUrl: 'downloads.html',
})
export class DownloadsPage {

  topicDownloads: Array<IDownload> = [];
  currentDownload: IDownload
  courseType: string;
  section: string;
  fullscreen: boolean = false;

  @ViewChild('appPlayerComponent') appPlayerComponent: PlayerComponent;


  constructor(public navCtrl: NavController, public navParams: NavParams, public dataProvider: DataProvider,
              private file: File,
              private fileManager: FileManagerProvider,
              private actionSheetCtrl: ActionSheetController,
              private orientation: OrientationProvider) {
    this.courseType = this.navParams.get('type');
  }

  loadTopicDownloads() {
    return this.dataProvider.getTopicDownloads(this.section).then((topicDownloads) => {
      this.topicDownloads = topicDownloads.filter((d: IDownload) => d.video.status == 2);
    });
  }


  removeDownload(download) {
    this.fileManager.removeDownload(download.topicId).then(() => {
      this.loadTopicDownloads();
    });
  }

  removeAllRegularCoursesDownloads() {
    let topicIds = this.topicDownloads.filter(topic=> topic.courseType == "regular_course" ).map(download => download.topicId)
    this.fileManager.removeDownloadList(topicIds).then(() => {
      this.loadTopicDownloads();
    }).catch(() => {
      this.loadTopicDownloads();
    })
  }
  removeAllCramCoursesDownloads() {
    let topicIds = this.topicDownloads.filter(topic=> topic.courseType == "cram_course" ).map(download => download.topicId)
    this.fileManager.removeDownloadList(topicIds).then(() => {
      this.loadTopicDownloads();
    }).catch(() => {
      this.loadTopicDownloads();
    })
  }

  ionViewDidEnter() {
    this.fileManager.init();
    this.orientation.setVerticalLock();
    this.dataProvider.loadCurrentSectionSku().then((section) => {
      this.section = section;
      this.loadTopicDownloads();
    });
  }

  hasDownloads(courseType) {
    return this.topicDownloads.find((download => download.courseType == courseType));
  }

  play(download: IDownload) {
    //console.log('video Play');
    //console.log(download);
    this.orientation.setHorizontalLock();
    this.fullscreen = true;
    this.currentDownload = download;
    setTimeout(() => {
      if (this.appPlayerComponent) {
        // Update player
        this.appPlayerComponent.start(this.currentDownload.video.videoId, true );
        let localFile = this.fileManager.getUrlFilename(this.currentDownload.pdf.localUrl);
        let path = this.fileManager.getUrlPath(this.currentDownload.pdf.localUrl);
        this.appPlayerComponent.loadPdfData({offline: true, path: path, localFile: localFile})
      }
    }, 100);

  }

  viewPDF(download: IDownload) {
    //console.log('viewPDF', download);
    if (download.pdf.localUrl) {
      let localFile = this.fileManager.getUrlFilename(download.pdf.localUrl);
      let path = this.fileManager.getUrlPath(download.pdf.localUrl);


      this.navCtrl.setRoot(PdfPage, {
        offline: true,
        path: path,
        localFile: localFile,
        displayTitle: download.title,
        referer: "DownloadsPage"
      });
    } else {
      console.error('PDF: no local url');
    }
  }


  /**
   * Listener to fire when a player goes full screen
   *
   * @param $event:IPlayerStatus
   */
  onFullScreen($event: IPlayerStatus) {
    // Back from fullscreen
    if (!($event && $event.isFullscreen)) {
      this.orientation.setVerticalLock();
      this.fullscreen = false;
      if (this.appPlayerComponent) {
        this.appPlayerComponent.pause();
      }
      this.currentDownload = null;
    }
  }


  presentDeleteConfirm(download: IDownload) {

    const actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to remove this download ? You will no longer be able to access it offline. ',
      buttons: [{
        cssClass: "remove",
        text: 'Remove Download',
        handler: () => {
          this.removeDownload(download);
        }
      }, {
        cssClass: "cancel",
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }
      ]
    });
    actionSheet.present();
  }


  presentDeleteAllRegularConfirm() {

    const actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to remove all course downloads? This will delete all downloads associated with this course.',
      buttons: [{
        cssClass: "remove",
        text: 'Remove all course downloads',
        handler: () => {
          this.removeAllRegularCoursesDownloads()
        }
      }, {
        cssClass: "cancel",
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }
      ]
    });
    actionSheet.present();
  }

  presentDeleteAllCramConfirm() {

    const actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to remove all course downloads? This will delete all downloads associated with this course.',
      buttons: [{
        cssClass: "remove",
        text: 'Remove all course downloads',
        handler: () => {
          this.removeAllCramCoursesDownloads()
        }
      }, {
        cssClass: "cancel",
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          //console.log('Cancel clicked');
        }
      }
      ]
    });
    actionSheet.present();
  }


}
