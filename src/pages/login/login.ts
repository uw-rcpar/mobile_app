import {Component, OnDestroy} from '@angular/core';
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {NavController, NavParams, AlertController, LoadingController, Loading} from 'ionic-angular';
import {HomePage} from "../home/home";
import {NgForm} from '@angular/forms';
import {ExternalLinksProvider} from "../../providers/external-links/external-links";
import {TourPage} from "../tour/tour";
import {NetworkProvider} from "../../providers/network/network";
import {Environment} from "../../environments/env";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  email = '';
  password = '';
  loading: Loading;
  submitted = false;
  baseSiteUrl = Environment.SITE_BASE_URL;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public userLoginProvider: UserLoginProvider,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public externalLinksProvider: ExternalLinksProvider,
              public networkProvider: NetworkProvider) {
  }

  showNoNetwork() {
    let alert = this.alertCtrl.create({
      title: "You're offline",
      subTitle: 'Please connect to the internet before attempting to log in',
      buttons: ['OK']
    });
    alert.present();
  }


  ionViewDidLoad() {
    if (!this.userLoginProvider.isLoggedIn() && this.userLoginProvider.firstLogin) {
      this.navCtrl.push(TourPage)
    }
  }


  public createAccount() {
    this.navCtrl.push('RegisterPage');
  }

  public login(form: NgForm) {
    if (this.networkProvider.hasNetwork == "online") {
      this.showLoading();
      this.submitted = true;
      if (form.valid) {
        this.userLoginProvider.login({email: this.email, password: this.password}).subscribe(
          () => {
            this.navCtrl.setRoot(HomePage);
          },
          (error) => {
            this.showError(error.error, error.message);
          }
        )

      }
    } else {
      this.showNoNetwork();
    }

  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(title: string, text: string) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

  openExternalLink(url: string) {
    this.externalLinksProvider.open2(url);
    return false;
  }

}
