export class Environment {
  public static RUNNING_ON_PLATFORM = window['cordova'] ? true : false;
  public static API_BASE_URL = "https://n3xjkdejog.execute-api.us-east-1.amazonaws.com";
  public static API_ENDPOINT = Environment.RUNNING_ON_PLATFORM ? ( Environment.API_BASE_URL + "/api/" ) : "/proxy/api/";
  public static  SITE_BASE_URL = "https://www.rogercpareview.com";

}
