import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Gesture, NavParams} from 'ionic-angular';
import {ElementRef} from '@angular/core';
import {File} from "@ionic-native/file";
import {Observable} from 'rxjs/Observable';
import {Subscription} from "rxjs";
import {auditTime} from 'rxjs/operators';


@Component({
  selector: 'pdf-reader',
  templateUrl: 'pdf-reader.html'
})

export class PdfReaderComponent implements OnInit, OnDestroy {
  @Input() public pdfUrl: string;
  @Input() public pdfPath: string;
  @Input() public pdfFile: string;
  @Input() public offline: boolean;
  public zoom = 1;

  /**
   * @type {number}
   * The wrapper zoom its just a css zoom property around the pdf viewer pages
   * It doesn't affect total canvas memory as it does the zoom property, but keeps the original image resolution
   * So may affect image quality if high values are applied
   */
  public wrapperZoom = 1;
  private pressGesture: Gesture;
  private ZOOM_DELTA = 0.25;
  private MAX_VIEWER_ZOOM = 1.5;
  private MAX_WRAPPER_ZOOM = 4;
  private WRAPPER_ZOOM_ENABLED = true;
  private ZOOM_GESTURE_THROTTLE_TIME = 250;
  private gestureObserverSubscription: Subscription;


  constructor(public navParams: NavParams, private elementRef: ElementRef, private file: File) {
  }

  public ngOnInit(): void {
    this.offline = this.navParams.get('offline');
    if (this.offline) {
      this.pdfPath = this.navParams.get('path');
      this.pdfFile = this.navParams.get('localFile');
      this.loadLocalPdf(this.pdfPath, this.pdfFile);
    } else {
      this.pdfUrl = this.navParams.get('url');
    }

    this.pressGesture = new Gesture(this.elementRef.nativeElement);
    this.pressGesture.listen();

    //We moved out from `this.pressGesture.on('pinch', (e: Event) => this.pinchEvent(e));` because its sending too many events
    let gestureObserver = Observable.fromEvent(this.pressGesture, 'pinch').pipe(
      auditTime(this.ZOOM_GESTURE_THROTTLE_TIME)
    );
    this.gestureObserverSubscription = gestureObserver.subscribe((e) => this.pinchEvent(e));
  }

  pinchEvent(e) {
    if (e['additionalEvent'] == "pinchout") {
      this.zoomIn();
    } else if (e['additionalEvent'] == "pinchin") {
      this.zoomOut();
    }
  }

  /**
   * Zooms in and out based on a combined zoom strategy: apply real zoom until MAX_VIEWER_ZOOM
   * Then increment wrapperZoom until reaches MAX_WRAPPER_ZOOM
   * Do the opposite order on zoom out: first reduce wrapperZoom until 1, then reduce zoom until min value
   */
  zoomIn() {
    if (this.zoom + this.ZOOM_DELTA <= this.MAX_VIEWER_ZOOM) {
      // Normal zoom (affects internal memory)
      this.zoom += this.ZOOM_DELTA;
    } else {
      // Wrapper zoom
      if (this.WRAPPER_ZOOM_ENABLED) {
        if (this.wrapperZoom + this.ZOOM_DELTA <= this.MAX_WRAPPER_ZOOM) {
          this.wrapperZoom += this.ZOOM_DELTA;
        }
      }
    }
  }

  zoomOut() {
    // Only zoom out until original size 1
    if (this.WRAPPER_ZOOM_ENABLED) {
      if (this.wrapperZoom > 1) {
        this.wrapperZoom -= this.ZOOM_DELTA;
        return;
      }
    }
    if (this.zoom > 1) {
      this.zoom -= this.ZOOM_DELTA;
    }
  }

  loadRemotePdf(url) {
    this.pdfUrl = url;
  }

  loadLocalPdf(path, filename) {
    this.file.readAsDataURL(path, filename).then((file) => {
      let reader = new FileReader();
      reader.onloadend = (e: any) => {
        this.pdfUrl = e.target.result;
      };
      let blob = new Blob([file], {type: "application/pdf"});
      reader.readAsBinaryString(blob);
    })
  }


  public ngOnDestroy(): void {
    // Destroy event listeners and observers when leaving the page
    this.gestureObserverSubscription.unsubscribe();
    this.pressGesture.destroy();
  }

}
