import {Component, Input} from '@angular/core';
@Component({
  selector: 'top-bar',
  templateUrl: 'top-bar.html'
})
export class TopBarComponent {
  @Input() public pageTitle: string;
  @Input() public sectionName: string = '';
  @Input() public showLogoSeparator: boolean = true;

}
