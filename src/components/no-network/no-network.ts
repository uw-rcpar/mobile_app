import {Component, Input, OnInit} from '@angular/core';
import {NavController} from "ionic-angular";
import {DownloadsPage} from "../../pages/downloads/downloads";
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {ISection} from "../../interfaces/ISection";

@Component({
  selector: 'no-network',
  templateUrl: 'no-network.html'
})
export class NoNetworkComponent implements OnInit {

  @Input() public sections: ISection[];

  public allowedDownloads: boolean = false;

  constructor(public navCtrl: NavController, private userLoginProvider: UserLoginProvider) {
  }

  public goToDownloads() {
    this.navCtrl.setRoot(DownloadsPage);
  }

  public ngOnInit() {
    if (this.sections && this.sections.length) {
      this.sections.forEach((section: ISection) => {
        if (this.userLoginProvider.hasOfflineAccessPermissions(section)) {
          this.allowedDownloads = true;
          // Allowed downloads if has downloads permissions either on cram or review course
        }
      });
    }
  }
}
