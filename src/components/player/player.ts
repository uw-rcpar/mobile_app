import {Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef, OnDestroy} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {throttleTime} from 'rxjs/operators';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import {VideoDataProvider} from "../../providers/video-data/video-data";
import {IPlayerStatus} from "../../interfaces/IPlayerStatus";
import {OrientationProvider} from "../../providers/orientation/orientation";
import {MenuProvider} from "../../providers/menu/menu";
import {ITopic} from "../../interfaces/ITopic";
import {FileManagerProvider} from "../../providers/file-manager/file-manager";
import {PdfReaderComponent} from "../pdf-reader/pdf-reader";
import {Subscription} from "rxjs";

@Component({
  selector: 'player',
  templateUrl: 'player.html',

})
export class PlayerComponent implements OnInit, OnDestroy {

  @ViewChild('appPlayer') appPlayer: ElementRef;
  @ViewChild('pdfReader') pdfReaderComponent: PdfReaderComponent;

  @Output() onFullScreen: EventEmitter<any> = new EventEmitter();
  @Output() onLoadedData: EventEmitter<any> = new EventEmitter();
  @Output() onPlaying: EventEmitter<any> = new EventEmitter();

  @Input() public videoUrl: string;
  @Input() public videoTitle: string;
  @Input() public pdfUrl: string;
  @Input() public pdfPath: string;
  @Input() public pdfFile: string;
  @Input() public offline: boolean;

  public videoType: string = 'video/mp4';
  private videoId: string;
  private autoplay: boolean;

  public showControls = true;
  public showEbook = false;
  private timeInterval;

  videoData: any = {};

  // Save track info every 10 secs
  VIDEO_TIME_UPDATE_EVENT = 10000;
  VIDEO_ALWAYS_AUTO_PLAY = false;

  public fullscreen: boolean = false;
  public isPlaying: boolean = false;
  public timeCurrent: number = 0;
  public timeTotal: number = 0;
  public timeLeft: number = 0;
  private _video: any;
  private isPlayingHolder: boolean;
  private debouncedObserverSubscription: Subscription;
  public waiting = true;

  private topic: ITopic;


  constructor(public videoDataProvider: VideoDataProvider, public orientationProvider: OrientationProvider, public menuProvider: MenuProvider, private fileManager: FileManagerProvider) {
    this.orientationProvider.change.subscribe(() => {
      this.fullscreen = this.orientationProvider.isHorizontal();
    })
  }

  /**
   * Return private video property
   *
   * @returns {any}
   */
  get video(): any {
    return this._video;

  }

  public start(videoId: string, autoplay: boolean) {
    //console.log(videoId)
    //console.log(this.videoId)
    //console.log(autoplay)
    this.isPlaying = true;
    this.autoplay = autoplay;
    if (this.videoId == videoId) {
      this.waiting = false;
      if(this.autoplay){
        this.play();
      }
    }else {
      //console.log("loading form 0")
      this.videoId = videoId;
      this.waiting = true;
      this.videoDataProvider.loadVideoData(videoId).then((videoData) => {
        this.videoData = videoData;
        let video = this.appPlayer.nativeElement;
        if (video) {
          //console.log("video Load!");
          video.load();
        }
      });
    }


  }


  /**
   * User requesting to switch in and out of full screen mode
   *
   * @emits IPlayerStatus
   */
  toggleFullscreen() {
    this.fullscreen = !this.fullscreen;
    this.showEbook = false;
    if (this.fullscreen) {
      this.menuProvider.disabledMenuSwap();
    } else {
      this.pause();
      this.menuProvider.enableMenuSwap();
    }
    this.onFullScreen.emit({isFullscreen: this.fullscreen});
    //console.log("toggleFullscreen", this.fullscreen);
  }

  /**
   * Click handler for clicking play or pause, usually only one or the other shows on screen
   */
  togglePlayPause() {
    this.doShowControls();
    // check if we're paused or playing, and update global status of play or pause status
    this.isPlaying = !this.appPlayer.nativeElement.paused;

    // play or pause based on status
    if (this.isPlaying) {
      this.pause();
    } else {
      this.play();
    }
  }

  /**
   * Pause the video
   */
  public pause() {
    if (this.appPlayer) {
      this.appPlayer.nativeElement.pause();
      this.isPlaying = false;
      //this.onPlaying.emit(this.isPlaying )
    }
  }

  /**
   * Play the video
   */
  public play() {
    //console.log("player set isPlaying = true");
    this.doShowControls();
    this.isPlaying = true;
    if (this.appPlayer) {
      this.appPlayer.nativeElement.play();
      //this.isPlaying = true;
      //this.onPlaying.emit(this.isPlaying )
    }
  }

  private updateVideoMeta() {
    let videoElem = this.appPlayer.nativeElement;
    this.timeCurrent = videoElem.currentTime.toFixed(1);
    this.timeTotal = videoElem.duration.toFixed(1);
    this.timeLeft = this.timeTotal - this.timeCurrent;
  }

  /**
   * Executes the first time the components its instantiated
   */
  ngOnInit() {
    // Video playback observer
    let videoElem = this.appPlayer.nativeElement;
    let videoObserver = Observable.fromEvent(videoElem, 'timeupdate');
    let debouncedObserver = videoObserver.pipe(throttleTime(this.VIDEO_TIME_UPDATE_EVENT));

    this.debouncedObserverSubscription = debouncedObserver.subscribe((e) => {
      if (this.videoId) {
        this.videoDataProvider.updateVideoTime(this.videoId, videoElem.currentTime * 1000);
      }

    });


    videoElem.volume = 1;

    // Start from last playbacktime if exists
    videoElem.addEventListener('loadedmetadata', () => {
      //console.log("EVENT: loadedmetadata");
      //console.log(this.videoData);
      if (this.videoData && this.videoData.playbackTime) {
        //console.debug('settingCurrentTime for video: ' + this.videoId, this.videoData.playbackTime);
        if (!isNaN(this.videoData.playbackTime)) {
          videoElem.currentTime = this.videoData.playbackTime / 1000;
        } else {
          //console.log('setting current time 0');
          videoElem.currentTime = 0;
        }
      }
      if (this.autoplay) {
        this.play();
      }
      this.waiting = false;
    }, false);

    // Attempt to disable the native player controls (e.g. hide the Shadow DOM elements,)
    // the reason for this is to prevent native full-screen, but also to add our own themed controls
    videoElem.addEventListener("contextmenu", (e) => {
      //console.log('contextmenu click');
      e.preventDefault();
      e.stopPropagation();
    }, false);

    if (videoElem.hasAttribute("controls")) {
      videoElem.removeAttribute("controls");
    }

    videoElem.addEventListener('click', (e) => {
      // should use CSS transitions, and add/remove a class to the toolbars to animate them in and out
      //console.log('videoElem click, @todo: hide/show the menu when in landscape based on click of the video');
    }, false);

    // Start from last playbacktime if exists
    videoElem.addEventListener('loadeddata', () => {
      //console.log("EVENT: loadeddata");
      this.updateVideoMeta();
      this.onLoadedData.emit();
    }, false);


    // videoElem.addEventListener('click', () => {
    //   if (this.fullscreen) {
    //     // on fullscreen mode handle click on the video // Workaround hidden button TODO fix this
    //     this.toggleFullscreen();
    //   }
    // }, false);

    videoElem.addEventListener('pause', () => {
      this.videoDataProvider.updateVideoTime(this.videoId, videoElem.currentTime * 1000);
      this.isPlaying = false;
      this.onPlaying.emit(this.isPlaying)
    }, false);

    videoElem.addEventListener('ended', () => {
      //console.log('EVENT: video ended');
      this.isPlaying = false;
      this.onPlaying.emit(this.isPlaying)
    }, false);

    videoElem.addEventListener('play', () => {
      //console.log('EVENT: video play');
      this.isPlaying = true;
      this.onPlaying.emit(this.isPlaying)
    }, false);

    videoElem.addEventListener('playing', () => {
      //console.log('EVENT: video playing');
      this.isPlaying = true;
      //this.onPlaying.emit(this.isPlaying )
    }, false);

    videoElem.addEventListener('timeupdate', () => {
      this.updateVideoMeta();
    }, false);
  }

  public ngOnDestroy() {
    this.debouncedObserverSubscription.unsubscribe();
  }

  public goBackward(): void {
    this.appPlayer.nativeElement.currentTime -= 10;
  }

  rangeBlur() {
    this.appPlayer.nativeElement.currentTime = this.timeCurrent;
    if (this.isPlayingHolder) {
      this.appPlayer.nativeElement.play();
    }
  }

  rangeFocus() {
    this.isPlayingHolder = this.isPlaying;
    this.appPlayer.nativeElement.pause();
  }

  doShowControls() {
    this.showControls = true;
    clearInterval(this.timeInterval);
    this.timeInterval = setInterval(() => {
      this.showControls = false
    }, 5000);
  }

  toggleEbook() {
    this.showEbook = !this.showEbook;
    if (this.showEbook) {
      //console.log(this.showEbook, this.fullscreen)
      if (this.offline) {
        this.pdfReaderComponent.loadLocalPdf(this.pdfPath, this.pdfFile)
      } else {
        this.pdfReaderComponent.loadRemotePdf(this.pdfUrl);
      }
    }
  }


  loadPdfData(data) {
    this.pdfUrl = data.url;
    this.pdfFile = data.localFile;
    this.pdfPath = data.path;
    this.offline = data.offline;
  }
}
