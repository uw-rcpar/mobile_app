import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { PlayerComponent } from './player/player';
import { MultilevelPushMenuComponent } from './multilevel-push-menu/multilevel-push-menu';
import { IonicModule } from "ionic-angular";
import {PipesModule} from "../pipes/pipes.module";
import { PdfReaderComponent } from './pdf-reader/pdf-reader';
import { TopBarComponent } from './top-bar/top-bar';
import {PdfViewerModule} from "ng2-pdf-viewer";
import { NoNetworkComponent } from './no-network/no-network';

@NgModule({
	declarations: [
	  PlayerComponent,
    MultilevelPushMenuComponent,
    PdfReaderComponent,
    TopBarComponent,
    NoNetworkComponent,
    ],
	imports: [
	  BrowserModule,
	  CommonModule,
    IonicModule,
    PipesModule,
    PdfViewerModule
  ],
	exports: [
	  PlayerComponent,
    MultilevelPushMenuComponent,
    PdfReaderComponent,
    TopBarComponent,
    NoNetworkComponent,
  ]
})
export class ComponentsModule {}
