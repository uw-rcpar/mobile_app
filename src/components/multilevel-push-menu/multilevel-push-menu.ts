import {Component, Input, ViewChild, EventEmitter, Output} from '@angular/core';
import {Slides, AlertController} from "ionic-angular";
import {SettingsPage} from "../../pages/settings/settings";
import {UserLoginProvider} from "../../providers/user-login/user-login";
import {ExternalLinksProvider} from "../../providers/external-links/external-links";
import {NotificationProvider} from "../../providers/notification/notification";

@Component({
  selector: 'multilevel-push-menu',
  templateUrl: 'multilevel-push-menu.html'
})
export class MultilevelPushMenuComponent {

  @Input()
  private _items: Array<any> = [];
  @Input() subItems: Array<any>;
  @Input() firstLevelRoot: any;
  @Input() secondLevelRoot: any;
  @Output() onMenuItemSelected: EventEmitter<any> = new EventEmitter();

  @ViewChild("menuSlider") menuSlider: Slides;

  public available_items: Array<any>;
  public disabled_items: Array<any>;

  @Input()
  set items(value: Array<any>) {
    this._items = value.filter(item => item.enabled)
  }


  get items(): Array<any> {
    return this._items;
  }

  menuLevel: Number = 1;


  constructor(private alertCtrl: AlertController, private userProvider: UserLoginProvider, public externalLinksProvider: ExternalLinksProvider, public notificationProvider: NotificationProvider) {
    this.subItems = [];
    this.firstLevelRoot = null;
    this.secondLevelRoot = null;
  }

  /**
   * Click handler for to open a Section (AUD, BEC, FAR, REG)
   *
   * @param sectionItem An item that represents a section
   */
  openSection(sectionItem) {
    // level 1 item clicked, so nav to level 2
    this.firstLevelRoot = sectionItem;
    // tell the component we're going to the second menu level
    this.menuLevel = 2;
    // ensure sidebar slides are unlocked
    this.menuSlider.lockSwipes(false);
    // slide to second page in sidebar slides
    this.menuSlider.slideTo(1);

    let menuItem = {
      "item": sectionItem,
      "level": 1
    };

    this.onMenuItemSelected.emit(menuItem);
  }

  /**
   * Click handler for Section Sub Item (Downloads, Regular Course...)
   *
   * @param item
   */
  openSectionItem(item) {
    //console.log('ITEM !:', item);
    // level 2 item clicked
    // go to the page provided via item
    this.secondLevelRoot = item;

    let menuItem = {
      "item": item,
      "level": 2
    };


    this.onMenuItemSelected.emit(menuItem);

    if (item.activated) {
      return false;
    }
  }

  /**
   * Special item to load settings page.
   */
  openSettingsPage() {
    // item to represent settings page
    let menuItem = {
      "item": {
        root: true,
        component: SettingsPage,
        enabled: true
      }
    };

    this.onMenuItemSelected.emit(menuItem);
  }

  checkActiveItem(item, level) {
    let active;
    if (level == 1) {
      active = this.firstLevelRoot == item;
    } else {
      active = this.secondLevelRoot == item;
    }
    //console.log(item.title, active);
    return active;
  }

  /**
   * The 'back' button on the second slide.
   * @TODO: A more elegant way if ever we add more slides would be to get index and slide back to that index
   */
  gotoPrevLevel() {
    this.menuLevel = 1;
    this.menuSlider.slidePrev();
  }

  /**
   * Fired when the user opens the menu, we'll make sure the user sees the correct sidebar menu slider page
   */
  menuOpened() {
    this.menuSlider.update();
    //console.log('menuOpened', this.menuLevel, this.firstLevelRoot);
    if (this.menuLevel == 1 && !this.firstLevelRoot) {
      // user does not yet have a section selected, so we need to lock swipes and nav to first slide
      this.menuSlider.lockSwipes(true);
      this.menuSlider.slideTo(0);
    }
  }


  notAllowed(i) {
    if (this.menuLevel == 2) {
      let section = this.firstLevelRoot.section;
      let featureId = i.id;
      let url = this.userProvider.getInterruptPageUrl(section, featureId);
      this.externalLinksProvider.open(url);
    }
  }

  notActivated(i) {
    //console.log("notActivated", i);
    if (this.menuLevel == 2) {
      // let section = this.firstLevelRoot.section;
      // let featureId = i.id;
      // let url = this.userProvider.getInterruptPageUrl(section, featureId + '-activate');
      // this.externalLinksProvider.open(url);
      this.showMessage()
    }
  }


  hasUpgrades(level) {
    let list: Array<any>;
    if (level == 1) {
      list = this.items;
    }
    if (level == 2) {
      list = this.subItems;
    }
    return list.filter((el) => !el.enabled).length > 0
  }

  initialize() {
    //this.menuLevel = 1 ;
    //this.firstLevelRoot = null ;
    this.menuLevel = 1;
    this.firstLevelRoot = null;
    this.secondLevelRoot = null;
    if (this.menuSlider.getActiveIndex() > 0) {
      this.menuSlider.slideTo(0, 0);
    }

  }


  showMessage() {
    const confirm = this.alertCtrl.create({
      cssClass: "access-restricted-message",
      title: "Activation Required",
      message: "Your Cram Course needs to be activated from your student dashboard on the website in order to view it in the app.",
      buttons: ['OK']
    });
    confirm.present();
  }
}
