import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {DownloadsPage} from '../pages/downloads/downloads';
import {HomePage} from '../pages/home/home';
import {ProgressPage} from '../pages/progress/progress';
import {LoginPage} from '../pages/login/login';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {DataProvider} from '../providers/data/data';
import {UserLoginProvider} from '../providers/user-login/user-login';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';
import {VideoDataProvider} from '../providers/video-data/video-data';
import {PipesModule} from "../pipes/pipes.module";
import {ComponentsModule} from "../components/components.module";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {NotificationsPage} from "../pages/notifications/notifications";
import {FileTransfer} from "@ionic-native/file-transfer";
import {File} from "@ionic-native/file";
import {Network} from '@ionic-native/network';
import {FileManagerProvider} from '../providers/file-manager/file-manager';
import {BootProvider} from '../providers/boot/boot';
import {VideosPage} from "../pages/videos/videos";
import {UserLoginApiProvider} from '../providers/user-login/user-login-api';
import {UserLoginLocalProvider} from '../providers/user-login/user-login-local';
import {PlatformOnlyProvider} from '../providers/platform-only/platform-only';
import {ToastProvider} from '../providers/toast/toast';
import {LoaderProvider} from '../providers/loader/loader';
import {AuthInterceptorProvider} from "../interceptors/authInterceptor";
import {NotificationProvider} from '../providers/notification/notification';
import {NotificationApiProvider} from '../providers/notification/notification-api';
import {NotificationLocalProvider} from '../providers/notification/notification-local';
import {NotificationViewPage} from "../pages/notification-view/notification-view";
import {ProgressProvider} from '../providers/progress/progress';
import {PdfPage} from "../pages/pdf/pdf";
import {OrientationProvider} from '../providers/orientation/orientation';
import {MomentModule} from 'angular2-moment';
import { MenuProvider } from '../providers/menu/menu';
import {PdfViewerModule} from "ng2-pdf-viewer";
import { ExternalLinksProvider } from '../providers/external-links/external-links';
import {TourPage} from "../pages/tour/tour";
import { NetworkProvider } from '../providers/network/network';
import {SettingsPage} from "../pages/settings/settings";
import { DataUsageWarningProvider } from '../providers/data-usage-warning/data-usage-warning';


@NgModule({
  declarations: [
    MyApp,
    DownloadsPage,
    HomePage,
    ProgressPage,
    LoginPage,
    NotificationsPage,
    NotificationViewPage,
    SettingsPage,
    VideosPage,
    PdfPage,
    TourPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      navBar: {alignTitle: "left"}
    }),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    ComponentsModule,
    PipesModule,
    MomentModule,
    PdfViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DownloadsPage,
    HomePage,
    ProgressPage,
    LoginPage,
    NotificationsPage,
    NotificationViewPage,
    VideosPage,
    PdfPage,
    TourPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    VideoDataProvider,
    ScreenOrientation,
    FileTransfer,
    File,
    Network,
    FileManagerProvider,
    BootProvider,
    UserLoginApiProvider,
    UserLoginLocalProvider,
    UserLoginProvider,
    PlatformOnlyProvider,
    ToastProvider,
    LoaderProvider,
    AuthInterceptorProvider,
    NotificationProvider,
    NotificationApiProvider,
    NotificationLocalProvider,
    ProgressProvider,
    OrientationProvider,
    ExternalLinksProvider,
    MenuProvider,
    NetworkProvider,
    DataUsageWarningProvider
  ]
})
export class AppModule {
}
