import {DownloadsPage} from "../pages/downloads/downloads";
import {SettingsPage} from "../pages/settings/settings";
import {ProgressPage} from "../pages/progress/progress";
import {VideosPage} from "../pages/videos/videos";
import {NotificationsPage} from "../pages/notifications/notifications";


// Static sub-navigation for all sections
// @todo: Each section will have different menu's based on the user entitlements, it may be worth working in
//        nav params to each page, so that the section is in the param object, this will aid in 'deep-links' later

export const available_pages = [
  {
    id: 'progress',
    title: 'Progress',
    description: "",
    component: ProgressPage,
    icon: "custom-progress",
    root: true,
    params: {},
    enabled: false, // Its always enabled independent of the entitlements
  },
  {
    id: 'review',
    title: 'Review Course',
    description: "",
    component: VideosPage,
    icon: "custom-review",
    root: true,
    params: {type: "regular_course"},
    enabled: false,
    entitlementTypes: ['Online Course']
  },
  {
    id: 'cram',
    title: 'Cram Course',
    description: "",
    component: VideosPage,
    icon: "custom-cram",
    root: true,
    params: {type: "cram_course"},
    enabled: false,
    entitlementTypes: ['Online Cram Course']
  },
  {
    id: 'download',
    title: 'Downloads',
    description: "",
    component: DownloadsPage,
    icon: "custom-downloads",
    root: true,
    params: {},
    enabled: false,
    entitlementTypes: []
  },
  {
    id: 'notifications',
    title: 'Notifications',
    description: "",
    component: NotificationsPage,
    icon: "custom-notifications",
    root: true,
    params: {},
    enabled: false
  },
  {
    id: 'settings',
    title: 'Settings',
    description: "",
    component: SettingsPage,
    icon: "custom-settings",
    root: false,
    params: {},
    enabled: false
  }


];
