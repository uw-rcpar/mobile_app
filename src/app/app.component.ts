import {Component, ViewChild} from '@angular/core';
import {Events, MenuController, Nav, Platform} from 'ionic-angular';
import {UserLoginProvider} from "../providers/user-login/user-login";
import {HomePage} from "../pages/home/home";
import {LoginPage} from "../pages/login/login";
import {MultilevelPushMenuComponent} from "../components/multilevel-push-menu/multilevel-push-menu";
import {DataProvider} from "../providers/data/data";

import {IPageItem} from "../interfaces/IPageItem";
import {BootProvider} from "../providers/boot/boot";
import {ISection} from "../interfaces/ISection";
import {IUser} from "../interfaces/IUser";
import {available_pages} from "./pages";
import {NotificationProvider} from "../providers/notification/notification";
import {INotification} from "../interfaces/INotification";
import {MenuProvider} from "../providers/menu/menu";
import {StatusBar} from '@ionic-native/status-bar';
import {Network} from "@ionic-native/network";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild("multilevelPushMenu") multilevelPushMenu: MultilevelPushMenuComponent;
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  sections: ISection[];
  pages: Array<IPageItem>;
  currentSection: {title: string, description: any};
  user: IUser;
  menuSwapStatus: string;


  constructor(private userLoginProvider: UserLoginProvider,
              private dataProvider: DataProvider,
              private bootProvider: BootProvider,
              private platform: Platform,
              private notificationProvider: NotificationProvider,
              public menuCtrl: MenuController,
              public menuProvider: MenuProvider,
              private statusBar: StatusBar) {
    this.pages = available_pages;
    this.initializeApp();
    this.menuProvider.change.subscribe(status => {
      this.menuSwapStatus = status;
      this.enableOrDisableMenuSwap()
    })
  }

  private enableOrDisableMenuSwap() {
    if (this.menuSwapStatus == "disabled") {
      this.menuCtrl.swipeEnable(false);
    } else {
      this.menuCtrl.swipeEnable(true);
    }
  }

  ngAfterViewInit() {
    this.nav.viewDidEnter.subscribe((data) => {
      this.enableOrDisableMenuSwap();
    })
  }


  openSection(section) {
    this.currentSection = section;
  }


  initializeApp() {

    this.userLoginProvider.user$.subscribe(user => {
      let alreadyLoggedIn = user && this.user;
      /**
       * For login flow, after login into the app alreadyLoggedIn = false
       * For token refresh flow alreadyLoggedIn = true
       * @type {T}
       */
      this.user = user;
      if (this.userLoginProvider.isLoggedIn()) {
        // Logged in
        //console.log('is loggin in');
        if (!alreadyLoggedIn) {
          this.bootProvider.sync().then(() => {
            this.rootPage = HomePage;
            this.notificationProvider.perform();
          });
        }
        return this.loadSections();

      } else {
        // Not logged in
        this.multilevelPushMenu.initialize();
        //console.debug('not logged in');
        this.rootPage = LoginPage;

      }
    });


    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#000000');
      if (this.userLoginProvider.isLoggedIn()) {
        this.bootProvider.boot().then(() => {
          this.rootPage = HomePage;
          this.loadSections();
        })
      }
    });
  }

  /**
   * Load the data for section-type entitlements
   **/
  loadSections() {
    this.dataProvider.getUserAllowedSections().then((sections: ISection[]) => {

      let order = ['AUD', 'BEC', 'FAR', 'REG'];
      this.sections = sections.sort((a, b) => {
        if (order.indexOf(a.section) < order.indexOf(b.section)) {
          return -1;
        } else if ((order.indexOf(a.section) > order.indexOf(b.section))) {
          return 1;
        }
        return 0;

      });
    });
  }

  loadEnabledPagesForSection(sectionSku: string) {
    let entitlements = this.user.entitlements;
    //Find under entitlements references to the current page

    this.pages = this.pages.map((p: IPageItem) => {
      // 1. Start all disabled
      p.enabled = false;

      // 2. Enabling the pages that are always enabled for any section , independent on the user entitilements
      if (['progress', 'notifications', 'settings'].indexOf(p.id) != -1) {
        p.enabled = true;
        p.activated = true;
      } else if (p.id == 'download') {
        // 3 For downloads need to check a special field on the entitlement list
        p.enabled = !!(
          this.userLoginProvider.hasOfflineAccessPermissions(this.dataProvider.findSectionBySkuAndType(sectionSku, 'regular_course')) ||
          this.userLoginProvider.hasOfflineAccessPermissions(this.dataProvider.findSectionBySkuAndType(sectionSku, 'cram_course'))
        );
        p.activated = true;
      } else {

        // for cram_corse and regular_course just check entitlement type field
        // 4. Checking entitlements, enabled the corresponding page
        // Criteria: Entitlement.type (cram, regular..) == Page.entitlement type for the current section
        const entitlement = (entitlements).find(e =>
              // Find entitlement by section sku, type
            e.section == sectionSku &&
            p.entitlementTypes &&
            p.entitlementTypes.indexOf(e.type) != -1
        );

        if (entitlement) {
          p.enabled = true;
          //console.log("Entitlement found for page ", p, entitlement);
          p.activated = entitlement.activated;
        }else{
          //console.log('entitlement not found for page' ,p );
        }
      }

      return p;
    });
    //console.debug('pages: ', this.pages);
  }

  menuOpened() {
    this.multilevelPushMenu.menuOpened();
  }

  /**
   * Main Handler for the menu selections
   * @param params
   */
  onMenuItemSelected(params) {
    // menu item
    if (params.item) {
      let item = params.item;

      // Our usage its always set the component on second level clicked
      if (item.component) {
        let params = item.params;
        //console.log(item);
        //section = this.sections.find((s) => s.type == 'regular_course' && s.section == item.section);
        if (item.root) {
          this.nav.setRoot(item.component, params);
        } else {
          this.nav.push(item.component, params);
        }
      }

      if (params.level == 1) {
        this.setCurrentSectionSku(item.section);
      }
    }
  }


  setCurrentSectionSku(sectionSku: string) {
    this.dataProvider.setCurrentSectionSku(sectionSku);
    this.loadEnabledPagesForSection(sectionSku);
  }

}
